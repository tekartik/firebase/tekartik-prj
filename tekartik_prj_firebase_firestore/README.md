## All in one

Firebase and simulator client for the web and io

```yaml
dependencies:
  tekartik_prj_firebase_firestore:
    git:
      url: git@gitlab.com:tekartik/firebase/tekartik-prj
      path: tekartik_prj_firebase_firestore
      ref: dart3a
    version: '>=0.1.0'
```

## Test