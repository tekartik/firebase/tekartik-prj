// ignore_for_file: depend_on_referenced_packages

import 'package:tekartik_firebase/firebase.dart';
import 'package:tekartik_firebase_auth_rest/auth_rest.dart';
import 'package:tekartik_firebase_client_browser_base/net/net_context.dart';
import 'package:tekartik_firebase_firestore_rest/firestore_rest.dart';
import 'package:tekartik_firebase_rest/firebase_rest.dart';
import 'package:tekartik_http_browser/http_client_browser.dart';

NetContext getNetContextRestWeb({AppOptions? options, String? appName}) {
  return NetContext(
      appOptions: options,
      appName: appName,
      firestoreService: firestoreServiceRest,
      firebase: firebaseRest,
      httpClientFactory: httpClientFactoryBrowser,
      authService: authServiceRest);
}

var netContextBrowser = NetContext(
    firestoreService: firestoreServiceRest,
    firebase: firebaseRest,
    httpClientFactory: httpClientFactoryBrowser,
    authService: authServiceRest);
