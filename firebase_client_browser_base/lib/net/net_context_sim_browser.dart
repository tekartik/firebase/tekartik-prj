import 'package:tekartik_firebase_auth_local/auth_local.dart';
import 'package:tekartik_firebase_client_browser_base/net/net_context.dart';
import 'package:tekartik_firebase_client_browser_base/src/import.dart';
import 'package:tekartik_firebase_client_browser_base/src/import_firebase.dart';
import 'package:tekartik_firebase_firestore_sim/firestore_sim.dart';
import 'package:tekartik_firebase_sim_browser/firebase_sim_client_browser.dart';
import 'package:tekartik_http_browser/http_client_browser.dart';

Firebase getFirebaseSimBrowser(
    {WebSocketChannelClientFactory? clientFactory, String? url}) {
  clientFactory ??= webSocketClientChannelFactoryBrowser;
  Firebase firebase = FirebaseSim(clientFactory: clientFactory, url: url);
  return firebase;
}

var netContextSimBrowser = NetContext(
    firestoreService: firestoreServiceSim,
    firebase: getFirebaseSimBrowser(url: 'ws://localhost:4996'),
    httpClientFactory: httpClientFactoryBrowser,
    authService: authServiceLocal);
