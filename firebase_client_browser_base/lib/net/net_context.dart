// Serve json context

import 'package:tekartik_firebase_client_browser_base/src/import_firebase.dart';
import 'package:tekartik_http/http_client.dart';

class NetContext {
  /// firebase app
  final AppOptions? appOptions;

  /// firebase app
  final String? appName;
  final FirestoreService firestoreService;
  final HttpClientFactory httpClientFactory;
  final AuthService? authService;
  final Firebase? firebase;
  final App app;

  Firestore? _firestore;

  Firestore get firestore => _firestore ??= firestoreService.firestore(app);
  Auth? _auth;

  Auth? get auth => _auth ??= authService?.auth(app);

  NetContext(
      {this.appName,
      this.appOptions,
      this.firebase,
      App? app,
      this.authService,
      required this.firestoreService,
      required this.httpClientFactory})
      : app =
            app ?? firebase!.initializeApp(options: appOptions, name: appName);
}
