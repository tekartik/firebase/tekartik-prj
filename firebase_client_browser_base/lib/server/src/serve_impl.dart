import 'dart:async';

import 'package:tekartik_firebase_client_browser_base/server/serve.dart'
    as common;
import 'package:tekartik_firebase_client_browser_base/src/import.dart';
import 'package:tekartik_firebase_client_browser_base/src/import_firebase.dart';
import 'package:tekartik_firebase_client_browser_base/src/import_io.dart';
import 'package:tekartik_firebase_firestore_sembast/firestore_sembast.dart';
import 'package:tekartik_firebase_firestore_sembast/firestore_sembast_io.dart';
import 'package:tekartik_firebase_firestore_sim/src/firestore_sim_server.dart'; // ignore: implementation_imports
import 'package:tekartik_firebase_sim/firebase_sim_server.dart';
import 'package:tekartik_firebase_sim_io/firebase_sim_client_io.dart' as sim;

class Context implements common.FirebaseServerContext {
  late FirebaseSimServer simServer;
  late Firebase firebase;

  @override
  Future close() async {
    await closeImpl(this);
  }
}

// using real websocker
Future<Context> initAndServerSimIo({int? port}) async {
  var testContext = Context();
  testContext.simServer =
      await serve(FirebaseLocal(), webSocketChannelFactoryIo, port: port);
  testContext.firebase = sim.getFirebaseSim(
      clientFactory: webSocketChannelClientFactoryIo,
      url: testContext.simServer.webSocketChannelServer.url);
  FirestoreSimServer(
      firestoreServiceIo, testContext.simServer, testContext.firebase);
  return testContext;
}

// memory only
Future<Context> initAndServerSim() async {
  var testContext = Context();
  // The server use firebase io
  testContext.simServer =
      await serve(FirebaseLocal(), webSocketChannelFactoryMemory);
  testContext.firebase = sim.getFirebaseSim(
      clientFactory: webSocketChannelClientFactoryMemory,
      url: testContext.simServer.webSocketChannelServer.url);
  FirestoreSimServer(
      firestoreServiceMemory, testContext.simServer, testContext.firebase);
  return testContext;
}

Future<Context> firebaseServeIo({int? port}) async {
  port ??= firebaseSimDefaultPort;
  var context = await initAndServerSimIo(port: port);
  print('serving on port ${context.simServer.url}');
  return context;
}

Future closeImpl(Context testContext) async {
  await testContext.simServer.close();
}
