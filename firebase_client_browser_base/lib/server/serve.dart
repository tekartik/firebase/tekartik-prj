import 'package:tekartik_firebase_client_browser_base/server/src/serve_impl.dart';

abstract class FirebaseServerContext {
  Future close();
}

/// Serve locally
Future<FirebaseServerContext> serve({int? port}) => firebaseServeIo(port: port);
