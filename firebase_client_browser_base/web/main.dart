import 'package:cv/cv.dart';
import 'package:path/path.dart';
import 'package:tekartik_firebase_client_browser_base/net/net_context.dart';
import 'package:tekartik_firebase_client_browser_base/net/net_context_sim_browser.dart';
import 'package:tekartik_firebase_client_browser_base/src/import_firebase.dart';
import 'package:tekartik_test_menu_browser/test_menu_mdl_browser.dart';

Future main() async {
  await initTestMenuBrowser();

  NetContext netContext;

  late DocumentReference docRev;
  enter(() {
    netContext = netContextSimBrowser;
    docRev = netContext.firestore.doc(url.join('test', 'localhost', 'write'));
  });
  item('write tests', () async {
    await docRev.set(Model.from({'text': 'value'}));
    write('done');
  });
  item('read tests', () async {
    var result = (await docRev.get()).dataOrNull;
    write(result ?? '<null>');
  });
  item('delete tests', () async {
    await docRev.delete();
    write('done');
  });
  item('prompt', () async {
    write('RESULT prompt: ${await prompt('Some text please then [ENTER]')}');
  });
}
