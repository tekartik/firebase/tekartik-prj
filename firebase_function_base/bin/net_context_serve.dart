import 'package:tekartik_common_utils/common_utils_import.dart';
import 'package:tekartik_firebase_function_base/demo.dart' as demo;
import 'package:tekartik_firebase_function_base/io_serve.dart';
import 'package:tekartik_firebase_function_base/net/net_context_io.dart';
import 'package:tekartik_firebase_functions_io/firebase_functions_io.dart';

Future main() async {
  //init();
  var netContext = netContextIo;
  demo.addDemoFunction(netContext, 'demo_out');
  serve().unawait();
  await firebaseServeIo();
}
