import 'package:path/path.dart' hide Context;
import 'package:tekartik_firebase_auth_local/auth_local.dart';
import 'package:tekartik_firebase_function_base/io_serve.dart' as fb;
import 'package:tekartik_firebase_function_base/src/import_firebase.dart';
import 'package:tekartik_firebase_function_base/users.dart' as users;
import 'package:tekartik_firebase_functions_io/firebase_functions_io.dart';

var deployTopPath = join('..', 'deploy');

Future main() async {
  //init();

  var firebaseLocal = FirebaseLocal();
  var app = firebaseLocal.initializeApp();
  users.addListUsersFunction(
      users.Context(
          firebaseFunctions: firebaseFunctionsIo,
          authService: authServiceLocal,
          app: app,
          handleCors: true),
      'users');
  await serve();
  /*var context =*/
  await fb.firebaseServeIo();
}
