import 'dart:io';

import 'package:grinder/grinder.dart';
import 'package:path/path.dart';
import 'package:tekartik_build_utils/cmd_run.dart';
import 'package:tekartik_build_utils/firebase/firebase_cmd.dart';

// ignore_for_file: non_constant_identifier_names
Future main(List<String> args) => grind(args);

@Task()
Future test() => TestRunner().testAsync();

String buildFolder =
    join('.dart_tool', 'tekartik_firebase_function_base', 'build');

String projectIdDev = 'tekartik-free-dev';
String projectId = projectIdDev;

@DefaultTask()
Future firebase_serve() async {
  await runCmd(PubCmd([
    'run',
    'build_runner',
    'build',
    '--output',
    'node_functions:$buildFolder'
  ]));
  copy(File(join(buildFolder, 'index.dart.js')), Directory('functions'));
  await runCmd(FirebaseCmd(firebaseArgs(serve: true, onlyFunctions: true)));
}

@Task()
Future firebase_deploy() async {
  // firebase deploy --only functions:func1,functions:func2
  await runCmd(FirebaseCmd(firebaseArgs(projectId: projectId, deploy: true)
    ..addAll(['--only', 'functions:mustacheDemo,functions:cms'])));
}

@Task()
void clean() => defaultClean();
