# Firebase function bases

Common base for creating cloud functions for firebase using dart, testing them on
dart vm and deploying on firebase

## All in one

Firebase cloud function and simulator dependencies

```yaml
dependencies:
  tekartik_firebase_function_base:
    git:
      url: git@gitlab.com:tekartik/firebase/tekartik-prj
      path: firebase_function_base
      ref: dart3a
    version: '>=0.1.0'
```

### Test Links

* <http://localhost:4999/demo_out>

curl -X '{"some":"data'}' http://localhost:4999?dev