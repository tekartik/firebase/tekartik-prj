import 'package:tekartik_firebase_function_base/request.dart';
import 'package:tekartik_firebase_functions/firebase_functions.dart';
import 'package:test/test.dart';

class TestExpressHttpRequest implements ExpressHttpRequest {
  @override
  Map<String, dynamic>? body;

  @override
  late HttpHeaders headers;

  @override
  late String method;

  @override
  Uri get requestedUri => uri;

  @override
  late ExpressHttpResponse response;

  @override
  late Uri uri;
}

void main() {
  group('request', () {
    test('paramMap', () async {
      var request = Request(TestExpressHttpRequest()
        ..uri = Uri.parse('http://test?test2=22&test3=33')
        ..body = {'test1': 1, 'test2': 2});

      expect(request.paramMap, {'test2': 2, 'test3': '33', 'test1': 1});
      // Check second access by debugging
      expect(request.paramMap, {'test2': 2, 'test3': '33', 'test1': 1});
    });
  });
}
