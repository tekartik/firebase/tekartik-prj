import 'dart:convert';

import 'package:tekartik_firebase/firebase.dart';
import 'package:tekartik_firebase_auth_local/auth_local.dart';
import 'package:tekartik_firebase_function_base/users.dart' as users_fn;
import 'package:tekartik_firebase_functions_io/firebase_functions_io.dart';
import 'package:tekartik_firebase_local/firebase_local.dart';
import 'package:tekartik_http/http.dart';
import 'package:tekartik_http_io/http_client_io.dart';
import 'package:test/test.dart';

void main() {
  App? app;
  var firebase = FirebaseLocal();
  late HttpServer server;
  setUpAll(() async {
    app = firebase.initializeApp();
    var context = users_fn.Context(
        firebaseFunctions: firebaseFunctionsIo,
        authService: authServiceLocal,
        app: app);
    users_fn.addListUsersFunction(context, 'users');
    server = await serve(port: httpPortAny);
  });
  tearDownAll(() async {
    await server.close();
    await app!.delete();
  });
  group('users', () {
    test('users', () async {
      var http = httpClientFactoryIo.newClient();
      var response = await http.get(
          Uri.parse('http://localhost:${server.port}/users'),
          headers: {'x-user-data': 'test'});
      var map = jsonDecode(response.body) as Map;
      expect(map['pageToken'], isNotNull);
      expect(map.length, 2);
      http.close();
    });

    test('x-user-data', () async {
      var http = httpClientFactoryIo.newClient();
      var response = await http.get(
          Uri.parse('http://localhost:${server.port}/users'),
          headers: {'x-user-data': 'test'});
      expect(response.headers['x-user-data'], 'test');
      http.close();
    });

    test('version', () async {
      var http = httpClientFactoryIo.newClient();
      var response = await http
          .get(Uri.parse('http://localhost:${server.port}/users?dev'));
      var map = json.decode(response.body) as Map;
      print(response.body);
      expect(map['_version'], isNotNull);
      http.close();
    });
  });
}
