import 'dart:convert';

import 'package:path/path.dart';
import 'package:tekartik_firebase/firebase.dart';
import 'package:tekartik_firebase_auth/auth.dart';
import 'package:tekartik_firebase_auth_local/auth_local.dart';
import 'package:tekartik_firebase_firestore_sembast/firestore_sembast_io.dart';
import 'package:tekartik_firebase_function_base/site_users.dart'
    as site_users_fn;
import 'package:tekartik_firebase_functions_io/firebase_functions_io.dart';
import 'package:tekartik_firebase_local/firebase_local.dart';
import 'package:tekartik_http/http.dart';
import 'package:tekartik_http_io/http_client_io.dart';
import 'package:test/test.dart';

void main() {
  App? app;
  UserRecord? userRecord;
  var firebase = FirebaseLocal();
  late HttpServer server;
  var sitesPath = 'sites_dev';
  setUpAll(() async {
    app = firebase.initializeApp();
    var context = site_users_fn.SiteContext(
        firestoreService: firestoreServiceIo,
        firebaseFunctions: firebaseFunctionsIo,
        authService: authServiceLocal,
        app: app);
    site_users_fn.addListUsersFunction(context, 'users', sitesPath: sitesPath);
    site_users_fn.addSiteInfoFunction(context, 'siteInfo',
        sitesPath: sitesPath);
    site_users_fn.addCheckUserAccessFunction(context, 'userCheckAccess',
        sitesPath: sitesPath);

    var auth = authServiceLocal.auth(app!);
    userRecord = await auth.getUser('1');
    var path = url.join(sitesPath, 'test', 'users', userRecord!.uid);
    await context.firestoreService
        .firestore(app!)
        .doc(path)
        .set(<String, dynamic>{'isAdmin': true});

    server = await serve(port: httpPortAny);
  });
  tearDownAll(() async {
    await server.close();
    await app!.delete();
  });
  group('users', () {
    test('users_bad_param', () async {
      var http = httpClientFactoryIo.newClient();
      // Add ?dev to the url for more logs
      var response = await httpClientSend(http, httpMethodGet,
          Uri.parse('http://localhost:${server.port}/users'),
          headers: {'x-user-id': '1'});
      expect(response.statusCode, httpStatusCodeInternalServerError);

      response = await httpClientSend(
        http,
        httpMethodGet,
        Uri.parse('http://localhost:${server.port}/users/test?dev&ids=1'),
      );
      expect(response.statusCode, httpStatusCodeUnauthorized,
          reason: response.body);
      response = await httpClientSend(
          http,
          httpMethodGet,
          Uri.parse(
              'http://localhost:${server.port}/users/test?ids=1&x-user-id=12345689'));
      expect(response.statusCode, httpStatusCodeForbidden,
          reason: response.body);
      http.close();
    });

    test('users_test', () async {
      var http = httpClientFactoryIo.newClient();
      // Add ?dev to the url for more logs
      var response = await httpClientRead(
        http,
        httpMethodGet,
        Uri.parse(
            'http://localhost:${server.port}/users/test/?ids=${userRecord!.uid}&x-user-id=${userRecord!.uid}'),
      );

      var map = jsonDecode(response) as Map;
      expect((map['users'] as List).length, 1);

      response = await httpClientRead(http, httpMethodGet,
          Uri.parse('http://localhost:${server.port}/users/test?dev'),
          body: json.encode({
            'x-user-id': '1',
            'ids': ['1', '2']
          }));

      map = jsonDecode(response) as Map;
      expect((map['users'] as List).length, 2);
      http.close();
    });

    test('users_headers_test', () async {
      var http = httpClientFactoryIo.newClient();
      // Add ?dev to the url for more logs
      var response = await httpClientRead(
          http,
          httpMethodGet,
          Uri.parse(
              'http://localhost:${server.port}/users/test/?ids=${userRecord!.uid}'),
          headers: <String, String>{'x-user-id': userRecord!.uid});

      var map = jsonDecode(response) as Map;
      expect((map['users'] as List).length, 1);

      /*
      response = await httpClientRead(
          http, httpMethodGet, 'http://localhost:${server.port}/users/test?dev',
          body: json.encode({
            'x-user-id': '1',
            'ids': ['1', '2']
          }));

      map = jsonDecode(response) as Map;
      expect(map['users'].length, 2);
      */
      http.close();
    });

    test('user_check_access_bad_param', () async {
      var http = httpClientFactoryIo.newClient();
      // Add ?dev to the url for more logs
      var response = await httpClientSend(http, httpMethodGet,
          Uri.parse('http://localhost:${server.port}/userCheckAccess/test'),
          body: json.encode({'x-user-id': '_any', 'x-email': 'dummy'}));
      expect(response.statusCode, httpStatusCodeForbidden,
          reason: response.body);

      http.close();
    });
    test('user_check_access_ok', () async {
      var http = httpClientFactoryIo.newClient();
      // Add ?dev to the url for more logs
      var response = await httpClientRead(http, httpMethodGet,
          Uri.parse('http://localhost:${server.port}/userCheckAccess/test'),
          body:
              json.encode({'x-user-id': '_any', 'x-email': userRecord!.email}));

      var map = jsonDecode(response) as Map;
      expect(map['isAdmin'], isTrue);

      response = await httpClientRead(http, httpMethodGet,
          Uri.parse('http://localhost:${server.port}/userCheckAccess/test?dev'),
          body: json.encode({'x-user-id': userRecord!.uid, 'x-email': '_any'}));

      map = jsonDecode(response) as Map;
      expect(map['isAdmin'], isTrue);
      http.close();
    });

    test('x-user-data', () async {
      var http = httpClientFactoryIo.newClient();
      var response = await http.get(
          Uri.parse('http://localhost:${server.port}/users'),
          headers: {'x-user-data': 'test'});
      expect(response.headers['x-user-data'], 'test');
      http.close();
    });

    test('version', () async {
      var http = httpClientFactoryIo.newClient();
      var response = await http
          .get(Uri.parse('http://localhost:${server.port}/users?dev'));
      var map = json.decode(response.body) as Map;
      print(response.body);
      expect(map['_version'], isNotNull);
      http.close();
    });

    test('siteInfo', () async {
      var http = httpClientFactoryIo.newClient();
      // Add ?dev to the url for more logs
      var response = await httpClientRead(http, httpMethodGet,
          Uri.parse('http://localhost:${server.port}/siteInfo/test'),
          headers: <String, String>{'x-user-id': userRecord!.uid});

      var map = jsonDecode(response) as Map?;
      expect(map, {
        'site': 'test',
        'x-user-id': '1',
        'idToken': null,
        'decodedUid': null,
        'authRequestUserId': null
      });

      response = await httpClientRead(http, httpMethodGet,
          Uri.parse('http://localhost:${server.port}/siteInfo/test'),
          headers: <String, String>{'Authorization': 'Bearer 1'});

      map = jsonDecode(response) as Map?;
      expect(map, {
        'site': 'test',
        'x-user-id': null,
        'idToken': '1',
        'decodedUid': '1',
        'authRequestUserId': '1'
      });
    });
  });
}
