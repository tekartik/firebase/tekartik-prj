@TestOn('vm')
library demo_test;

import 'package:tekartik_common_utils/date_time_utils.dart';
import 'package:tekartik_firebase_function_base/demo.dart' as demo_fn;
import 'package:tekartik_firebase_function_base/net/net_context_io.dart';
import 'package:tekartik_firebase_functions_io/firebase_functions_io.dart';
import 'package:tekartik_http/http.dart';
import 'package:tekartik_http_io/http_client_io.dart';
import 'package:tekartik_io_utils/io_utils_import.dart';
import 'package:test/test.dart';

void main() {
  var netContext = netContextIo;
  late HttpServer server;
  setUpAll(() async {
    demo_fn.addDemoFunction(netContext, 'demo');
    server = await serve(port: httpPortAny);
  });
  tearDownAll(() async {
    await server.close();
  });
  group('demo', () {
    test('demo', () async {
      var http = httpClientFactoryIo.newClient();
      var response = await http.get(
          Uri.parse('http://localhost:${server.port}/demo'),
          headers: {'x-user-data': 'test'});
      var map = jsonDecode(response.body) as Map?;
      print(jsonPretty(map));
      //expect(map['pageToken'], isNotNull);
      //expect(map.length, 2);
      http.close();
    });

    test('version', () async {
      var http = httpClientFactoryIo.newClient();
      var response =
          await http.get(Uri.parse('http://localhost:${server.port}/demo?dev'));
      var map = json.decode(response.body) as Map;
      print(response.body);
      expect(map['_version'], isNotNull);
      var dateText = map['_date'] as String?;
      var date = parseDateTime(dateText)!;
      expect(date.isUtc, isTrue);
      var duration = parseInt(map['_duration']);
      expect(duration, greaterThan(-1));
      http.close();
    });
  });
}
