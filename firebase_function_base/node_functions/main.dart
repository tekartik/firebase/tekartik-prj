import 'package:tekartik_firebase_auth_node/auth_node.dart';
import 'package:tekartik_firebase_function_base/src/import_node.dart';
import 'package:tekartik_firebase_function_base/users.dart' as users_fn;
import 'package:tekartik_firebase_functions_node/firebase_functions_node.dart';

void main() {
  // node_interop.fs;
  var firebase = firebaseNode;

  var app = firebase.initializeApp();
  var context = users_fn.Context(
    app: app,
    firebaseFunctions: firebaseFunctionsNode,
    authService: authServiceNode,
  );
  users_fn.addListUsersFunction(context, 'users');
}
