import 'package:tekartik_firebase_function_base/src/request_mixin.dart';
import 'package:tekartik_firebase_functions/firebase_functions.dart';
import 'package:tekartik_http/http.dart';

class HtmlRequest with RequestMixin {
  HtmlRequest(ExpressHttpRequest httpRequest) {
    this.httpRequest = httpRequest;
    // _handleCommonQueryParameters();
  }

  Future sendResponse(String html, {int? statusCode}) async {
    await lock.synchronized(() async {
      if (!responseSent) {
        responseSent = true;

        if (statusCode != null) {
          httpRequest.response.statusCode = statusCode;
        }
        await _writeHtml(html);
      }
    });
  }

  Future _writeHtml(String html) async {
    /*
    if (dev) {
      map['_version'] = version.toString();
      map['_date'] = DateTime.now().toString();
    }
    var text = prettyPrint ? jsonPretty(map) : jsonEncode(map);
    var userData = httpRequest.headers.value(userDataHeader);
    if (userData != null) {
      httpRequest.response.headers.set(userDataHeader, userData);
    }
    */
    httpRequest.response.headers
        .set(httpHeaderContentType, httpContentTypeHtml);
    httpRequest.response.writeln(html);
    await httpRequest.response.close();
  }
}
