import 'package:tekartik_common_utils/common_utils_import.dart';
import 'package:tekartik_firebase_function_base/net/net_context.dart';
import 'package:tekartik_firebase_function_base/request.dart';
import 'package:tekartik_firebase_function_base/src/import_firebase.dart';
import 'package:tekartik_firebase_function_base/src/import_firebase.dart'
    as firebase;
import 'package:tekartik_firebase_functions/firebase_functions.dart';
import 'package:tekartik_http/http.dart';

void addDemoFunction(NetContext context, String name) {
  var usersApp = DemoApp(context);
  context.firebaseFunctions![name] =
      context.firebaseFunctions!.https.onRequest(usersApp.simpleOut);
}

class DemoApp {
  firebase.App get firebaseApp => context.app;
  Auth? auth;
  final NetContext context;

  DemoApp(this.context) {
    auth = context.authService!.auth(firebaseApp);
  }

  Future simpleOut(ExpressHttpRequest expressRequest) async {
    var result = <String, dynamic>{};

    var request = Request(expressRequest);
    var body = expressRequest.body;
    if ((body == null) || ((body is List) && (body.isEmpty))) {
      //ignore
    } else {
      // if (expressRequest.method == httpMethodPost) {
      if (request.dev) {
        result['_body'] = request.bodyMap;
      }
    }

    try {
      result['out'] = 'Message from ${DateTime.now()}';
      await request.sendResponse(result);
    } catch (e, st) {
      print(e);
      print(st);
      await request.sendError(
          httpStatusCodeInternalServerError, 0, e.toString(),
          stackTrace: st);
    }
  }
}
