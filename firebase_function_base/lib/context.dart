import 'package:tekartik_firebase_functions/firebase_functions.dart';
import 'package:tekartik_http/http_client.dart';

mixin FirebaseFunctionsMixin {
  FirebaseFunctions get firebaseFunctions;
}

mixin HttpClientFactoryMixin {
  HttpClientFactory get httpClientFactory;
}

// Serve json context
class Context {}
