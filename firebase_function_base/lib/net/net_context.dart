// Serve json context
import 'package:tekartik_firebase/firebase.dart';
import 'package:tekartik_firebase_auth/auth.dart';
import 'package:tekartik_firebase_firestore/firestore.dart';
import 'package:tekartik_firebase_functions/firebase_functions.dart';
import 'package:tekartik_http/http_client.dart';

class NetContext {
  final FirestoreService firestoreService;
  final FirebaseFunctions? firebaseFunctions;
  final HttpClientFactory httpClientFactory;
  final AuthService? authService;
  final Firebase? firebase;
  final App app;

  Firestore? _firestore;

  Firestore get firestore => _firestore ??= firestoreService.firestore(app);

  NetContext(
      {this.firebaseFunctions,
      this.firebase,
      App? app,
      this.authService,
      required this.firestoreService,
      required this.httpClientFactory})
      : app = app ?? firebase!.initializeApp();
}
