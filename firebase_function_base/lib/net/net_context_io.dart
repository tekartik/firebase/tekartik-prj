import 'package:tekartik_firebase_auth_local/auth_local.dart';
import 'package:tekartik_firebase_firestore_sembast/firestore_sembast_io.dart';
import 'package:tekartik_firebase_function_base/net/net_context.dart';
import 'package:tekartik_firebase_functions_io/firebase_functions_io.dart';
import 'package:tekartik_firebase_local/firebase_local.dart';
import 'package:tekartik_http_io/http_client_io.dart';

final firebaseLocal = FirebaseLocal();

var netContextIo = NetContext(
    firestoreService: firestoreServiceIo,
    firebaseFunctions: firebaseFunctionsIo,
    firebase: firebaseLocal,
    httpClientFactory: httpClientFactoryIo,
    authService: authServiceLocal);
