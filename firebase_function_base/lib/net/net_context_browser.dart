import 'package:tekartik_firebase_auth_node/auth_node.dart';
import 'package:tekartik_firebase_firestore_node/firestore_node.dart';
import 'package:tekartik_firebase_function_base/net/net_context.dart';
import 'package:tekartik_firebase_functions_node/firebase_functions_node.dart';
import 'package:tekartik_firebase_node/firebase_node.dart';
import 'package:tekartik_http_node/http_client_node.dart';

var netContextBrowser = NetContext(
    firestoreService: firestoreServiceNode,
    firebaseFunctions: firebaseFunctionsNode,
    firebase: firebaseNode,
    httpClientFactory: httpClientFactoryNode,
    authService: authServiceNode);
