import 'package:cv/cv.dart';
import 'package:path/path.dart' hide Context;
import 'package:tekartik_common_utils/common_utils_import.dart';
import 'package:tekartik_common_utils/list_utils.dart';
import 'package:tekartik_common_utils/string_utils.dart';
import 'package:tekartik_firebase_function_base/auth_request.dart';
import 'package:tekartik_firebase_function_base/request.dart';
import 'package:tekartik_firebase_function_base/users.dart';
import 'package:tekartik_firebase_functions/firebase_functions.dart';
import 'package:tekartik_http/http.dart';

import 'src/import_firebase.dart';
import 'src/import_firebase.dart' as firebase;

const String usersPart = 'users';
const String usersByEmailPart = 'users_by_email';
const String userIsAdminField = 'isAdmin';
const String userEmailField = 'email';

void addSiteInfoFunction(SiteContext context, String name,
    {required String sitesPath}) {
  var usersApp = SiteUsersApp(context, sitesPath: sitesPath);
  context.firebaseFunctions[name] =
      context.firebaseFunctions.https.onRequest(usersApp.siteInfoHttpHandler);
}

void addListUsersFunction(SiteContext context, String name,
    {required String sitesPath}) {
  var usersApp = SiteUsersApp(context, sitesPath: sitesPath);
  context.firebaseFunctions[name] =
      context.firebaseFunctions.https.onRequest(usersApp.listUsersHttpHandler);
}

void addCheckUserAccessFunction(SiteContext context, String name,
    {required String sitesPath}) {
  var usersApp = SiteUsersApp(context, sitesPath: sitesPath);
  context.firebaseFunctions[name] = context.firebaseFunctions.https
      .onRequest(usersApp.checkUserAccessHttpHandler);
}

class SiteUsersApp {
  final String sitesPath;

  SiteUsersApp(this.context, {required this.sitesPath}) {
    auth = context.authService.auth(firebaseApp!);
    firestore = context.firestoreService.firestore(firebaseApp!);
  }

  firebase.App? get firebaseApp => context.app;
  late Auth auth;
  late Firestore firestore;
  final SiteContext context;

  static String getSitePath(String sitesPath, String site) =>
      url.join(sitesPath, site);

  Future listUsersHttpHandler(ExpressHttpRequest expressRequest) async {
    var request = Request(expressRequest);

    if (request.handleCors(extraHeaders: ['x-user-id'])) {
      return;
    }

    var site = request.siteHost;
    var ids = request.getStringListParam('ids');

    var result = <String, dynamic>{};

    if (request.dev) {
      print('${expressRequest.method} ${expressRequest.uri}');
      // print(request.headers);
      print('site: $site, ids: $ids');
      result['site'] = site;
      result['ids'] = ids;
    }
    // devPrint(request.httpRequest.headers);
    var userId = (request.paramMap['x-user-id'] ??
        request.httpRequest.headers.value('x-user-id')) as String?;
    // TODO check user access or firebase auth

    var users = <Map<String, dynamic>?>[];
    try {
      if (stringIsEmpty(site)) {
        throw ArgumentError('site must not be null');
      }
      if (listIsEmpty(ids)) {
        throw ArgumentError('ids must not be null');
      }
      if (stringIsEmpty(userId)) {
        await request.sendError(
            httpStatusCodeUnauthorized, 0, 'Not authorized');
      }

      var path = url.join(sitesPath, site, usersPart, userId);
      var siteUserSnapshot = await firestore.doc(path).get();
      bool? isAdmin = false;
      if (siteUserSnapshot.exists) {
        var siteUser = siteUserSnapshot.data;
        isAdmin = parseBool(siteUser[userIsAdminField] ?? false);
      }
      if (!isAdmin!) {
        await request.sendError(httpStatusCodeForbidden, 0, 'Not authorized');
        return;
      }
      if (expressRequest.method == httpMethodGet) {
        for (var id in ids!) {
          try {
            var userRecord = await (auth.getUser(id) as FutureOr<UserRecord>);
            users.add(userRecordToJson(userRecord));
          } catch (e) {
            print('cannot find $id error $e [${e.runtimeType}]');
            users.add(null);
          }
          // userRecord can be null! we respond to all ids anyway
        }
        result[usersPart] = users;
        await request.sendResponse(result);
      } else {
        throw 'unsupported ${expressRequest.method}';
      }
    } catch (e, st) {
      print(e);
      print(st);
      await request.sendError(
          httpStatusCodeInternalServerError, 0, e.toString(),
          stackTrace: st, data: result);
    }
  }

  Future siteInfoHttpHandler(ExpressHttpRequest expressRequest) async {
    var request = Request(expressRequest);

    if (request.handleCors(extraHeaders: ['x-user-id'])) {
      return;
    }

    var site = request.siteHost;

    var result = <String, dynamic>{};

    if (request.dev) {
      print('${expressRequest.method} ${expressRequest.uri}');
      // print(request.headers);
    }
    result['site'] = site;
    var userId = (request.paramMap['x-user-id'] ??
        request.httpRequest.headers.value('x-user-id')) as String?;
    result['x-user-id'] = userId;
    try {
      var idToken = request.httpRequest.headers
          .value('Authorization')
          ?.split('Bearer')
          .last
          .trim();
      result['idToken'] = idToken;
      if (idToken != null) {
        var decoded = await auth.verifyIdToken(idToken);
        result['decodedUid'] = decoded.uid;
      } else {
        result['decodedUid'] = null;
      }
    } catch (e) {
      result['verifyIdTokenError'] = '$e (${e.runtimeType})';
    }
    result['authRequestUserId'] = await authRequestUserId(auth, request);

    try {
      if (expressRequest.method == httpMethodGet) {
        await request.sendResponse(result);
      } else {
        throw 'unsupported ${expressRequest.method}';
      }
    } catch (e, st) {
      print(e);
      print(st);
      await request.sendError(
          httpStatusCodeInternalServerError, 0, e.toString(),
          stackTrace: st, data: result);
    }
  }

  Future checkUserAccessHttpHandler(ExpressHttpRequest expressRequest) async {
    var request = Request(expressRequest);

    if (request.handleCors(extraHeaders: ['x-user-id', 'x-email'])) {
      return;
    }

    var site = request.siteHost;
    // devPrint('## PARAM ${request.paramMap}');
    // devPrint('## BODY ${request.bodyMap}');

    var result = <String, dynamic>{};
    var userId = request.paramMap['x-user-id']?.toString();
    var email = request.paramMap['x-email']?.toString();

    if (request.dev) {
      print('${expressRequest.method} ${expressRequest.uri}');
      // print(request.headers);
      print('site: $site, email: $email, uid: $userId');
      result['site'] = site;
      result['email'] = email;
      result['uid'] = userId;
    }
    // TODO check user access or firebase auth

    try {
      if (stringIsEmpty(site)) {
        throw ArgumentError('site must not be null');
      }
      if (stringIsEmpty(email)) {
        throw ArgumentError('email must not be null');
      }
      if (stringIsEmpty(userId)) {
        await request.sendError(
            httpStatusCodeUnauthorized, 0, 'Not authorized');
      }

      var userByEmailPath = url.join(sitesPath, site, usersByEmailPart);

      /*
      var siteUser = (await firestore.doc(path).get())?.data;
      if (siteUser == null || (!(parseBool(siteUser[userIsAdminField] ?? false)))) {
        await request.sendError(httpStatusCodeForbidden, 0, 'Not authorized');
        return;
      }
      */
      if (expressRequest.method == httpMethodGet) {
        var docs = (await firestore
                .collection(userByEmailPath)
                .where(userEmailField, isEqualTo: email)
                .get())
            .docs;
        String? userPath;
        UserRecord? authUser;
        if (docs.isNotEmpty) {
          var doc = docs.first;
          authUser = await auth.getUserByEmail(email!);
          if (authUser == null) {
            await request.sendError(
                httpStatusCodeNotFound, 0, 'email $email Not found');
            return;
          }
          userPath = url.join(sitesPath, site, usersPart, authUser.uid);
          await firestore.runTransaction((txn) {
            txn.set(firestore.doc(userPath!), docs.first.data,
                SetOptions(merge: true));
            txn.delete(doc.ref);
          });

          // var userRecord = await auth.getUserByEmail(email);
          // users.add(userRecordToJson(userRecord));
        }
        userPath ??= url.join(sitesPath, site, usersPart, userId);
        var siteUserSnapshot = await firestore.doc(userPath).get();
        bool? isAdmin = false;
        if (siteUserSnapshot.exists) {
          var siteUser = Model.from(siteUserSnapshot.data);
          isAdmin = (parseBool(siteUser[userIsAdminField] ?? false));
        }
        if (!isAdmin!) {
          authUser ??= await auth.getUserByEmail(email!);
          if (authUser != null && userId != authUser.uid) {
            userId = authUser.uid;
            userPath = url.join(sitesPath, site, usersPart, userId);
            siteUserSnapshot = await firestore.doc(userPath).get();
            if (siteUserSnapshot.exists) {
              var siteUser = asModel(siteUserSnapshot.data);
              isAdmin = parseBool(siteUser[userIsAdminField] ?? false)!;
            }
          }
        }
        if (!isAdmin && authUser == null) {
          var siteUserSnapshot =
              await firestore.doc(join(usersPart, userId)).get();
          if (siteUserSnapshot.exists) {
            // Check global user
            var globalUser = Model.from(siteUserSnapshot.data);
            isAdmin = parseBool(globalUser[userIsAdminField] ?? false)!;
          }
          if (!isAdmin) {
            await request.sendError(
                httpStatusCodeForbidden, 0, 'Not authorized');
            return;
          }
          var update = Model.from({userIsAdminField: isAdmin});
          await firestore.doc(userPath).set(update, SetOptions(merge: true));
        }

        result[userIsAdminField] = isAdmin;
        await request.sendResponse(result);
      } else {
        throw 'unsupported ${expressRequest.method}';
      }
    } catch (e, st) {
      print(e);
      print(st);
      await request.sendError(
          httpStatusCodeInternalServerError, 0, e.toString(),
          stackTrace: st, data: result);
    }
  }
}

// Serve json context
class SiteContext extends Context {
  final FirestoreService firestoreService;

  SiteContext(
      {super.handleCors,
      required super.firebaseFunctions,
      required super.app,
      required this.firestoreService,
      required super.authService});
}

// final Firestore firebaseFirestore;
