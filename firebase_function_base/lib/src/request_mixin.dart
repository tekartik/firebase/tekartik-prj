import 'package:synchronized/synchronized.dart';
import 'package:tekartik_firebase_functions/firebase_functions.dart';

mixin RequestMixin {
  // Response lock
  final lock = Lock();
  bool responseSent = false;

  /// Only in dev
  late Stopwatch stopwatch;

  // extra logs
  late bool dev;

  late ExpressHttpRequest httpRequest;

  Map<String, dynamic>? _paramMap;

  /// Convenient map from either the body (first) or the query params
  Map<String, dynamic> get paramMap => _paramMap ??= () {
        var map = <String, dynamic>{};
        map.addAll(httpRequest.uri.queryParameters);
        return map;
      }();
}
