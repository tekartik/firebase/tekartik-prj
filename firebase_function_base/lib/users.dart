import 'package:tekartik_common_utils/common_utils_import.dart';
import 'package:tekartik_common_utils/list_utils.dart';
import 'package:tekartik_firebase_function_base/request.dart';
import 'package:tekartik_firebase_functions/firebase_functions.dart';
import 'package:tekartik_http/http.dart';

import 'src/import_firebase.dart';
import 'src/import_firebase.dart' as firebase;

void addListUsersFunction(Context context, String name) {
  var usersApp = UsersApp(context);
  context.firebaseFunctions[name] =
      context.firebaseFunctions.https.onRequest(usersApp.listUsersHttpHandler);
}

class UsersApp {
  firebase.App? get firebaseApp => context.app;
  late Auth auth;
  final Context context;

  UsersApp(this.context) {
    auth = context.authService.auth(firebaseApp!);
  }

  Future listUsersHttpHandler(ExpressHttpRequest expressRequest) async {
    var request = Request(expressRequest);

    if (context.handleCors == true) {
      //request.response.headers.set(HttpHeaders.CONTENT_TYPE, "text/plain; charset=UTF-8");
      expressRequest.response.headers.add(
          'Access-Control-Allow-Methods', 'POST, OPTIONS, GET, PATCH, DELETE');
      expressRequest.response.headers.add('Access-Control-Allow-Origin', '*');
      expressRequest.response.headers.add('Access-Control-Allow-Headers',
          'Origin,Content-Type,Authorization,Accept,connection,content-length,host,user-agent'
          //redirectOptions.corsHeadersText
          );

      if (expressRequest.method == 'OPTIONS') {
        await expressRequest.response.send();
        /*
          ..statusCode = 200
          ..write('')
          ..close();
          */
        return;
      }
    }

    var maxResults = parseInt(expressRequest.uri.queryParameters['maxResults']);
    var pageToken = expressRequest.uri.queryParameters['pageToken'];
    var dev = parseBool(listFirst(expressRequest.headers['dev'])) ?? false;
    if (dev) {
      print('${expressRequest.method} ${expressRequest.uri}');
      // print(request.headers);
      print('maxResults: $maxResults, pageToken: $pageToken');
    }
    var result = <String, dynamic>{};
    var users = <Map<String, dynamic>>[];
    try {
      if (expressRequest.method == httpMethodGet) {
        var usersResult =
            await auth.listUsers(maxResults: maxResults, pageToken: pageToken);
        var done = usersResult.users.isEmpty;
        if (!done) {
          pageToken = usersResult.pageToken;
          result['pageToken'] = pageToken;
          if (usersResult.users.isEmpty) {
            done = true;
          } else {
            users.addAll(
                usersResult.users.map((user) => userRecordToJson(user!)));
          }
        }
        result['users'] = users;
        await request.sendResponse(result);
      } else {
        throw 'unsupported ${expressRequest.method}';
      }
    } catch (e, st) {
      print(e);
      print(st);
      await request.sendError(
          httpStatusCodeInternalServerError, 0, e.toString(),
          stackTrace: st);
    }
  }
}

// Serve json context
class Context {
  final FirebaseFunctions firebaseFunctions;
  final App? app;
  final AuthService authService;
  final bool? handleCors;

  Context(
      {this.handleCors,
      required this.firebaseFunctions,
      required this.app,
      required this.authService});
}
