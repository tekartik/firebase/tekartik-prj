import 'package:tekartik_firebase_firestore_sembast/firestore_sembast.dart';
import 'package:tekartik_firebase_firestore_sembast/firestore_sembast_io.dart';
// ignore_for_file: implementation_imports
import 'package:tekartik_firebase_firestore_sim/src/firestore_sim_server.dart';
import 'package:tekartik_firebase_function_base/src/import.dart';
import 'package:tekartik_firebase_function_base/src/import_firebase.dart';
import 'package:tekartik_firebase_function_base/src/import_io.dart';
import 'package:tekartik_firebase_sim/firebase_sim_server.dart';
import 'package:tekartik_firebase_sim_io/firebase_sim_client_io.dart' as sim;

class Context {
  late FirebaseSimServer simServer;
  late Firebase firebase;
}

// using real websocker
Future<Context> initAndServerSimIo({int? port}) async {
  var testContext = Context();
  testContext.simServer =
      await serve(FirebaseLocal(), webSocketChannelFactoryIo, port: port);
  testContext.firebase = sim.getFirebaseSim(
      clientFactory: webSocketChannelClientFactoryIo,
      url: testContext.simServer.webSocketChannelServer.url);
  FirestoreSimServer(
      firestoreServiceIo, testContext.simServer, testContext.firebase);
  return testContext;
}

// memory only
Future<Context> initAndServerSim() async {
  var testContext = Context();
  // The server use firebase io
  testContext.simServer =
      await serve(FirebaseLocal(), webSocketChannelFactoryMemory);
  testContext.firebase = sim.getFirebaseSim(
      clientFactory: webSocketChannelClientFactoryMemory,
      url: testContext.simServer.webSocketChannelServer.url);
  FirestoreSimServer(
      firestoreServiceMemory, testContext.simServer, testContext.firebase);
  return testContext;
}

/// Serve a simulated version of firebase (firestore) on localhost
Future<Context> firebaseServeIo({int? port}) async {
  port ??= firebaseSimDefaultPort;
  var context = await initAndServerSimIo(port: port);
  print('serving firebase on port ${context.simServer.url}');
  return context;
}

Future close(Context testContext) async {
  await testContext.simServer.close();
}
