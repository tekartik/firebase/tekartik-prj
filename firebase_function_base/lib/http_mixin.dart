import 'package:tekartik_firebase_function_base/net/net_context.dart';
import 'package:tekartik_firebase_function_base/src/import_firebase.dart';
import 'package:tekartik_firebase_function_base/src/import_firebase.dart'
    as firebase;
import 'package:tekartik_firebase_functions/firebase_functions.dart';

const String httpHeaderContentEncoding = 'Content-Encoding';
const String httpHeaderAcceptEncoding = 'Accept-Encoding';

void helloHandler(ExpressHttpRequest request) {
  print('request.url ${request.uri}');
  //print("request.originalUrl ${request.requestedUri}");
  request.response.writeln('NEW Hello from Firebase Functions Dart Interop!');
  request.response.writeln('uri: ${request.uri}');
  // request.response.writeln("requestedUri: ${request.requestedUri}");
  request.response.close();
}

class App {
  late firebase.App firebaseApp;
  final NetContext context;

  Firestore get firestore => context.firestoreService.firestore(firebaseApp);

  App(this.context) {
    print('index.main()');
    initDefault();
  }

  void initDefault() {
    context.firebaseFunctions!['hello'] =
        context.firebaseFunctions!.https.onRequest(helloHandler);

    firebaseApp = context.firebase!.initializeApp();
  }
}

App? app;

void run(NetContext context) {
  app = App(context);
}
