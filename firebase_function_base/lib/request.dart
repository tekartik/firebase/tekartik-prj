import 'dart:convert';

import 'package:pub_semver/pub_semver.dart';
import 'package:stack_trace/stack_trace.dart';
import 'package:tekartik_common_utils/bool_utils.dart';
import 'package:tekartik_common_utils/json_utils.dart';
import 'package:tekartik_firebase_function_base/src/request_mixin.dart';
import 'package:tekartik_firebase_functions/firebase_functions.dart';
import 'package:tekartik_http/http.dart';

const String userDataHeader = 'x-user-data';

const int errorCodeUnknown = 0;

final baseVersion = Version(0, 2, 0);

class FunctionException {
  final int statusCode;
  final int errorCode;
  final String message;
  final StackTrace? stackTrace;

  FunctionException(this.statusCode, this.errorCode, this.message,
      [this.stackTrace]);
}

class Request with RequestMixin {
  Request(ExpressHttpRequest httpRequest) {
    this.httpRequest = httpRequest;
    _handleCommonQueryParameters();
  }

  String get version => baseVersion.toString();

  late bool prettyPrint;

  String? _siteHost;

  /// If first part is the target domain
  String get siteHost => _siteHost ??= () {
        var path = httpRequest.uri.toString();
        if (path.startsWith('/')) {
          path = path.substring(1);
        }
        // First part or uri such 'testtekartik.wordpress.com'
        var host = path.split('/').first.split('?').first.split('#').first;
        return host;
      }();

  Map<String, dynamic>? _paramMap;

  /// Convenient map from either the body (first) or the query params
  @override
  Map<String, dynamic> get paramMap => _paramMap ??= () {
        var map = <String, dynamic>{};
        map.addAll(httpRequest.uri.queryParameters);
        if (bodyMap != null) {
          map.addAll(bodyMap!);
        }
        return map;
      }();

  Future sendResponse(Map<String, dynamic> map, {int? statusCode}) async {
    await lock.synchronized(() async {
      if (!responseSent) {
        responseSent = true;

        if (statusCode != null) {
          httpRequest.response.statusCode = statusCode;
        }
        try {
          await _writeMap(map);
        } catch (e, st) {
          await _sendError(httpStatusCodeInternalServerError, 0, '$e for map',
              data: map, stackTrace: st);
        }
      }
    });
  }

  Future _writeMap(Map<String, dynamic> map) async {
    if (dev) {
      map['_version'] = version.toString();
      map['_date'] = DateTime.now().toUtc().toIso8601String();
      map['_duration'] = (stopwatch..stop()).elapsedMilliseconds;
    }
    var text = prettyPrint ? jsonPretty(map)! : jsonEncode(map);
    var userData = httpRequest.headers.value(userDataHeader);
    if (userData != null) {
      httpRequest.response.headers.set(userDataHeader, userData);
    }
    httpRequest.response.headers
        .set(httpHeaderContentType, httpContentTypeJson);
    httpRequest.response.writeln(text);
    await httpRequest.response.close();
  }

  // Should use for standard error reporting
  Future sendError(int statusCode, int errorCode, String message,
      {StackTrace? stackTrace, Map<String, dynamic>? data}) async {
    await lock.synchronized(() async {
      if (!responseSent) {
        await _sendError(statusCode, errorCode, message,
            stackTrace: stackTrace, data: data);
      }
    });
  }

  Future _sendError(int statusCode, int errorCode, String message,
      {StackTrace? stackTrace, Map<String, dynamic>? data}) async {
    responseSent = true;
    var error = <String, dynamic>{'errorCode': errorCode, 'message': message};
    if (data?.isNotEmpty == true) {
      error['data'] = data;
    }
    if (dev && stackTrace != null) {
      error['stackTrace'] = Trace.format(stackTrace);
    }
    var map = <String, dynamic>{'statusCode': statusCode, 'error': error};

    httpRequest.response.statusCode = statusCode;
    try {
      await _writeMap(map);
    } catch (e) {
      error['data'] = data?.toString();
      try {
        await _writeMap(map);
      } catch (e) {
        await _writeMap(<String, dynamic>{'data': data?.toString()});
      }
    }
  }

  Future sendException(FunctionException functionException) => sendError(
      functionException.statusCode,
      functionException.errorCode,
      functionException.message,
      stackTrace: functionException.stackTrace);

  Map<String, dynamic>? _bodyMap;

  Map<String, dynamic>? get bodyMap =>
      _bodyMap ??= requestBodyAsJsonObject(httpRequest.body);

  bool _parseQueryBool(String key) {
    return httpRequest.uri.queryParameters.containsKey(key) &&
        parseBool(httpRequest.uri.queryParameters[key]) != false;
  }

  void _handleCommonQueryParameters() {
    prettyPrint = _parseQueryBool('pp');
    dev = _parseQueryBool('dev');
    if (dev) {
      stopwatch = Stopwatch()..start();
    }
  }

  Future handleException(Object e, StackTrace st) async {
    if (!responseSent) {
      if (e is FunctionException) {
        await sendException(e);
        return;
      }
      print(st);
      print(e);
      await sendError(httpStatusCodeInternalServerError, errorCodeUnknown,
          'internal error $e',
          stackTrace: st);
    }
  }

  /// Get a list parameter. If it is string, it is split by comma
  List<String>? getStringListParam(String key) {
    var param = paramMap[key];
    if (param is Iterable) {
      return param.map((item) => item.toString()).toList(growable: false);
    } else if (param is String) {
      return param.split(',');
    }
    return null;
  }

  /// Add CORS headers
  /// Return true it OPTIONS was handled
  bool handleCors({List<String>? extraHeaders}) {
    // Add headers
    httpRequest.response.headers.add(
        'Access-Control-Allow-Methods', 'POST, OPTIONS, GET, PATCH, DELETE');
    httpRequest.response.headers.add('Access-Control-Allow-Origin', '*');
    var acceptHeaders = <String>[
      'Origin',
      'Content-Type',
      'Authorization',
      'Accept',
      'connection',
      'content-length',
      'host',
      'user-agent',
      'x-recaptcha-token'
    ];
    if (extraHeaders != null) {
      acceptHeaders.addAll(extraHeaders);
    }
    httpRequest.response.headers
        .add('Access-Control-Allow-Headers', acceptHeaders.join(','));

    if (httpRequest.method == 'OPTIONS') {
      httpRequest.response.send('{}');
      return true;
    }

    return false;
  }
}
