import 'package:tekartik_firebase_function_base/request.dart';
import 'package:tekartik_firebase_function_base/src/import_firebase.dart';

Future<String?> authRequestUserId(Auth auth, Request request) async {
  try {
    var idToken = request.httpRequest.headers
        .value('Authorization')
        ?.split('Bearer')
        .last
        .trim();
    var decoded = await auth.verifyIdToken(idToken!);
    return decoded.uid;
  } catch (e) {
    print(e);
    return null;
  }
}
