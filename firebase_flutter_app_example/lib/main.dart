// ignore_for_file: depend_on_referenced_packages

import 'package:cv/cv.dart';
import 'package:path/path.dart';
import 'package:tekartik_firebase_client_browser_base/net/net_context.dart';
import 'package:tekartik_firebase_firestore/firestore.dart';
import 'package:tekartik_firebase_flutter_app_base/menu.dart';
import 'package:tekartik_firebase_flutter_app_base/menu/init.dart';
import 'package:tekartik_test_menu_flutter/demo/demo_test_menu_flutter.dart'
    as demo;

void main() {
  appPackageName = 'com.tekartik.firebase_flutter_app_example';
  // base.main();
  mainMenu(() {
    //devPrint('MAIN_');
    item('write', () {
      write('now is ${DateTime.now()}');
    });
    menu('Firestore', () {
      NetContext? netContext;
      late DocumentReference docRev;
      enter(() async {
        await appInfoReady;
        netContext = appInfo!.netContext;
        docRev =
            netContext!.firestore.doc(url.join('test', 'localhost', 'write'));
      });

      item('write tests', () async {
        await docRev.set(Model.from({'text': 'value'}));
        write('done');
      });
      item('read tests', () async {
        var result = (await docRev.get()).dataOrNull;
        write(result);
      });
      item('delete tests', () async {
        await docRev.delete();
        write('done');
      });
      item('prompt', () async {
        write(
            'RESULT prompt: ${await prompt('Some text please then [ENTER]')}');
      });
    });
    menu('demo', () {
      demo.main();
    });
  });
}
