// ignore_for_file: depend_on_referenced_packages

import 'dart:async';
import 'dart:typed_data';

import 'package:tekartik_firebase_firestore_node/firestore_universal.dart';
import 'package:tekartik_firebase_functions_node/firebase_functions_universal.dart';
import 'package:tekartik_firebase_node/firebase_universal.dart';
import 'package:tk_gcf_redirect/constant.dart';
import 'package:tk_gcf_redirect/functions.dart';
import 'package:tk_gcf_redirect/generate_id_function.dart';
import 'package:tk_gcf_redirect/src/http_redirect_impl.dart';
//import 'package:haw_football_gcf_functions/functions.dart';

Future main() async {
  print('starting...');
  var functions = firebaseFunctionsUniversal;
  firebaseFunctionsUniversal['testHttp'] = functions.https
      .onRequest(testHttp, httpsOptions: HttpsOptions(region: region));
  functions[generateIdFnName] = functions.https
      .onRequest(generateId, httpsOptions: HttpsOptions(region: region));

  firebaseFunctionsUniversal['helloEu'] = firebaseFunctions.https.onRequest(
      helloWorld,
      httpsOptions: HttpsOptions(
          region: region,
          timeoutSeconds: 90,
          memory: runtimeOptionsMemory128MB));

  try {
    firebaseFunctionsUniversal['testTrigger'] = firebaseFunctions.firestore
        .document('tests/tekartik_firebase/tests/test_in')
        .onWrite(triggerHandler);
    // At 18 10
    firebaseFunctionsUniversal['testCron'] = firebaseFunctions.scheduler
        .onSchedule(
            ScheduleOptions(
                region: region,
                schedule: '35 19 * * *',
                timeZone: 'Europe/Paris'),
            cronHandler);
  } catch (e) {
    print('error setting trigger and cron: $e');
  }
  var appRedirector = AppRedirector();
  functions[redirectFnName] =
      functions.https.onRequest(appRedirector.httpRedirectHandler);

  functions['echoQuery'] = firebaseFunctions.https.onRequest(echoQueryHandler);
  functions['echoBytes'] = firebaseFunctions.https.onRequest(echoBytesHandler);
  functions['echoFragment'] =
      firebaseFunctions.https.onRequest(echoFragmentHandler);
  functions['echoHeaders'] =
      firebaseFunctions.https.onRequest(echoHeadersHandler);

  await firebaseFunctionsUniversal.serve();
}

final firebaseApp = firebase.initializeApp();
final firestore = firestoreService.firestore(firebaseApp);

Future triggerHandler(
    Change<DocumentSnapshot> change, EventContext eventContext) async {
  print('Tracking change $change');
  await firestore
      .doc('tests/tekartik_firebase/tests/test_out')
      .set({'timestamp': FieldValue.serverTimestamp});
}

Future helloWorld(ExpressHttpRequest request) async {
  print('request.uri ${request.uri}');
  await request.response.send('2020-09-22 Hello');
}

FutureOr<void> cronHandler(ScheduleEvent context) {
  print('Schedule handler');
}

void echoBytesHandler(ExpressHttpRequest request) {
  // devPrint('request.body ${request.body?.runtimeType}: ${request.body}');
  request.response.send(request.body as Uint8List?);
}

void echoHandler(ExpressHttpRequest request) {
  // print("request.url ${request.uri}");
  //request.response.write(requestBodyAsText(request.body));
  //request.response.close();
  request.response.send(requestBodyAsText(request.body));
}

void echoQueryHandler(ExpressHttpRequest request) {
  // print("request.url ${request.uri} ${request.uri.queryParameters}");
  request.response.send(request.uri.query);
}

void echoFragmentHandler(ExpressHttpRequest request) {
  // print("request.url ${request.uri} ${request.uri.fragment}");
  request.response.send(request.uri.fragment);
}

void echoHeadersHandler(ExpressHttpRequest request) {
  // print("request.url ${request.uri} ${request.uri.fragment}");
  var sb = StringBuffer();
  request.headers.forEach((name, values) {
    sb.writeln('name: ${values.join(', ')}');
  });
  request.response.send(sb.toString());
}
