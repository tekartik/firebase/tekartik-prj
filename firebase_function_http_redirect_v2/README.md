Firebase will be setup in deploy folder

# Initial setup

- follow firebase function setup
- in deploy, run 'firebase init' (cloud functions)
- `bin/main.dart`
  ```dart
  import 'package:tekartik_firebase_functions_node/firebase_functions_universal.dart';
  
  Future main() {
    print('starting...');
    firebaseFunctions['helloWorld'] =
        firebaseFunctions.https.onRequest(helloWorld);
  }
  
  Future helloWorld(ExpressHttpRequest request) async {
    print('request.uri ${request.uri}');
    await request.response.send('2020-09-09 Hello');
  }
  ```