import 'package:tekartik_app_http/app_http.dart';
import 'package:tekartik_common_utils/common_utils_import.dart';

Future<void> main() async {
  var uri =
      Uri.parse('https://europe-west3-gael-dev.cloudfunctions.net/testHttp');
  var client = httpClientFactory.newClient();
  var result = await httpClientRead(client, httpMethodPost, uri,
      body: {'method': 'fs_get', 'path': 'info/app'});
  print(jsonPretty(result));
/*
  result = await httpClientRead(client, httpMethodPost, uri,
      body: utf8.encode(jsonEncode({'path': 'info/app'})));
  print(jsonPretty(result));

 */
}
