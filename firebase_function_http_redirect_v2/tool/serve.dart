import 'package:tekartik_app_node_build/gcf_build.dart';
import 'package:tk_gcf_redirect/constant.dart';

import 'project.dart';

Future serve() async {
  await gcfNodePackageServeFunctions(path, projectId: projectId);
}

Future main() async {
  await serve();
}
