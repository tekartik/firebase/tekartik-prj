import 'package:tekartik_app_node_build/gcf_build.dart';
import 'package:tk_gcf_redirect/constant.dart';

import 'rest_test.dart' as rest_test;

var path = '.';

Future<void> main() async {
  var sw = Stopwatch()..start();
  await gcfNodePackageBuild(path);
  await gcfNodePackageDeployFunctions(path,
      projectId: projectId, functions: [generateIdFnName]);
  sw.stop();
  print('Done: ${sw.elapsed}');
  await rest_test.main();
}
