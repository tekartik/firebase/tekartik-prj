import 'package:tekartik_app_node_build/gcf_build.dart';
import 'package:tk_gcf_redirect/constant.dart';

import 'build_and_deploy_firebase_function_generate_id.dart';

Future main() async {
  await gcfNodePackageBuild(path);
  await gcfNodePackageServeFunctions(path, projectId: projectId);
}
