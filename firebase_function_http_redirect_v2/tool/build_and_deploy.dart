import 'package:tekartik_app_node_build/gcf_build.dart';

var projectId = 'tekartik-net-dev';
var builder =
    GcfNodeAppBuilder(options: GcfNodeAppOptions(projectId: projectId));

Future main() async {
  await gcfNodePackageBuild('.');
  //await deploy();
}
