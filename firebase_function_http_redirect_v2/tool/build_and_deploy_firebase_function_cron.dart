import 'package:path/path.dart';
import 'package:process_run/shell.dart';
import 'package:tekartik_app_node_build/gcf_build.dart';

var devProjectId = 'tekartik-net-dev';

Future<void> firebaseFunctionsDeploy(String path,
    {required List<String> functions}) async {
  var shell = Shell(workingDirectory: join(path, 'deploy'));
  var projectId = devProjectId;
  await shell.run(
      'firebase --project $projectId deploy --only ${functions.map((e) => 'functions:$e').join(',')}');
}

Future<void> main() async {
  var sw = Stopwatch()..start();
  await gcfNodePackageBuild('.');
  await firebaseFunctionsDeploy('.', functions: ['testCron']);
  sw.stop();
  print('Done: ${sw.elapsed}');
}
