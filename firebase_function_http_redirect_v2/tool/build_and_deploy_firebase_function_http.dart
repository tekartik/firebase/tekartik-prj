import 'package:tekartik_app_node_build/gcf_build.dart';

import 'rest_test.dart' as rest_test;

var projectId = 'tekartik-net-dev';
var builder = GcfNodeAppBuilder(
    options: GcfNodeAppOptions(projectId: projectId, functions: ['testHttp']));

Future<void> main() async {
  var sw = Stopwatch()..start();
  await builder.buildAndDeployFunctions(functions: ['testHttp']);
  sw.stop();
  print('Done: ${sw.elapsed}');
  await rest_test.main();
}
