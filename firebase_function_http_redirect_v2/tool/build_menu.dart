import 'package:http/http.dart';
import 'package:tekartik_app_node_build/gcf_build.dart';
import 'package:tekartik_http_redirect/http_redirect.dart';
import 'package:tekartik_test_menu_io/test_menu_io.dart';
import 'package:tk_gcf_redirect/constant.dart';
import 'package:tk_gcf_redirect/src/http_redirect_client.dart';

import 'project.dart';

Future<void> deployGenerateId() async {
  await gcfNodePackageDeployFunctions(path,
      projectId: projectId, functions: [generateIdFnName]);
}

void main(List<String> args) {
  // TestMenuManager.debug.on = true;
  mainMenu(args, () {
    command((command) {
      print('Command entered: $command');
    });
    menu('main', () {
      item('build', () async {
        await gcfNodePackageBuild(path);
      }, cmd: 'b');
      item('deploy generatedIdv2', () async {
        await deployGenerateId();
      });
      item('build and deploy echo functions', () async {
        await gcfNodePackageBuild(path);
        await gcfNodePackageDeployFunctions(path,
            projectId: projectId,
            functions: [
              'echoQuery',
              'echoBytes',
              'echoFragment',
              'echoHeaders'
            ]);
      });
      item('rest generatedIdv2', () async {
        var result = await read(Uri.parse(
            'https://europe-west3-tekartik-net-dev.cloudfunctions.net/generateIdV2'));
        write(result);
      });
      item('rest redirectV2', () async {
        var uri = Uri.parse(
                'https://europe-west3-tekartik-net-dev.cloudfunctions.net/redirectV2')
            .replace(queryParameters: {
          redirectUrlHeader:
              'https://europe-west3-tekartik-net-dev.cloudfunctions.net/generateIdV2'
        });
        write(uri);
        var result = await read(uri);
        write(result);
      });
      item('rest redirect using api', () async {
        var client = TestRedirectClient(Client());
        var result = await client.read(Uri.parse(
            'https://europe-west3-tekartik-net-dev.cloudfunctions.net/generateIdV2'));
        write(result);
      });
      item('local node generatedIdv2', () async {
        var result = await read(
            Uri.parse('http://localhost:5000/$projectId/$region/generateIdV2'));
        write(result);
      });
      item('local dart generatedIdv2', () async {
        var result =
            await read(Uri.parse('http://localhost:4999/generateIdV2'));
        write(result);
      });
      item('local dart redirectV2', () async {
        var result = await read(Uri.parse('http://localhost:4999/redirectV2')
            .replace(queryParameters: {
          redirectUrlHeader: 'http://localhost:4999/generateIdV2'
        }));
        write(result);
      });
      item('local node redirectV2', () async {
        var result = await read(
            Uri.parse('http://localhost:5000/$projectId/$region/redirectV2')
                .replace(queryParameters: {
          redirectUrlHeader:
              'http://localhost:5000/$projectId/$region/generateIdV2'
        }));
        write(result);
      });
      item('start build and serve', () async {
        await gcfNodePackageBuild(path);
        gcfNodePackageServeFunctions(path, projectId: projectId).unawait();
        write('** STARTED **');
        //var shell = Shell(workingDirectory: join(path, 'deploy'));
        //await shell.run('firebase serve --projectId=$projectId');
      });
      menu('sub', () {
        item('print hi', () => print('hi'));
      }, cmd: 's');
      item('custom menu', () async {
        write('before custom menu');
        await showMenu(() {
          item('custom item', () {});
        });
        write('after custom menu');
      });
    });
  });
}
