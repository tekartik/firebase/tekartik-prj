import 'package:tekartik_common_utils/common_utils_import.dart';
import 'package:test/test.dart';
import 'package:tk_gcf_redirect/generate_id_client.dart';

Future<void> main() async {
  test('rest_test', () async {
    await callRestGenerateId(verbose: true);
    await callRestGenerateId(delayMs: 500, verbose: true);
    await callRestGenerateId(delayMs: 1000, verbose: true);
  });
/*
  result = await httpClientRead(client, httpMethodPost, uri,
      body: utf8.encode(jsonEncode({'path': 'info/app'})));
  print(jsonPretty(result));

 */
}
