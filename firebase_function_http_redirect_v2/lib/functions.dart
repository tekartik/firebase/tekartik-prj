// ignore_for_file: depend_on_referenced_packages

import 'package:cv/cv.dart';
import 'package:tekartik_firebase_firestore/utils/json_utils.dart';
import 'package:tekartik_firebase_firestore_node/firestore_universal.dart';
import 'package:tekartik_firebase_functions_node/firebase_functions_universal.dart';
import 'package:tekartik_firebase_node/firebase_universal.dart';

Future testHttp(ExpressHttpRequest request) async {
  /*
  var method = url.basename(request.uri.path);
  switch (method) {
    case 'fs_get':
      {
        var body = request.body;
        await request.response.send(body);
        return;
      }
  }
  print('request.uri ${request.uri}');
  await request.response.send('2020-09-22 Hello');

   */
  var body = request.body;
  print('$body ${body.runtimeType}');
  if (body is Map) {
    print('body is Map');
    var method = body['method'] as String;
    print('method: $method');
    switch (method) {
      case 'fs_get':
        {
          final firebaseApp = firebase.initializeApp();
          var firestore = firestoreService.firestore(firebaseApp);
          var path = body['path'] as String;
          var snapshot = (await firestore.doc(path).get());
          var json = snapshotToJson(snapshot);
          print('read: $json');
          await request.response.send(json);
          return;
        }
    }
  }
  await request.response.send('${body.runtimeType} $body');
}

Model snapshotToJson(DocumentSnapshot snapshot) {
  var map = <String, dynamic>{
    'exists': snapshot.exists,
    'path': snapshot.ref.path
  };
  if (snapshot.exists) {
    map['data'] = documentDataMapToJsonMap(snapshot.data);
    map['createTime'] = snapshot.createTime?.toIso8601String();
    map['updateTime'] = snapshot.updateTime?.toIso8601String();
  }
  return map;
}
