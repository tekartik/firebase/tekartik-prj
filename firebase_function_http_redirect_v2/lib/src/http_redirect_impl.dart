import 'package:http/http.dart';
import 'package:tekartik_common_utils/byte_utils.dart';
import 'package:tekartik_common_utils/common_utils_import.dart';
import 'package:tekartik_firebase/firebase.dart' as firebase;
import 'package:tekartik_firebase_functions/firebase_functions.dart';
import 'package:tekartik_http_node/http_client_universal.dart';
import 'package:tekartik_http_redirect/http_redirect.dart' as http_redirect;
import 'package:tekartik_http_redirect/http_redirect.dart';

var httpClientFactory = httpClientFactoryUniversal;

const String httpHeaderContentEncoding = 'Content-Encoding';
const String httpHeaderAcceptEncoding = 'Accept-Encoding';

void helloHandler(ExpressHttpRequest request) {
  print('request.url ${request.uri}');
  //print("request.originalUrl ${request.requestedUri}");
  request.response.writeln('NEW Hello from Firebase Functions Dart Interop!');
  request.response.writeln('uri: ${request.uri}');
  // request.response.writeln("requestedUri: ${request.requestedUri}");
  request.response.close();
}

Future httpRedirectHandler(ExpressHttpRequest request) async {}

///
/// Copied from http_redirect
///
/// Proxy the HTTP request to the specified server.
Future proxyExpressHttpRequest(
    Options? options, ExpressHttpRequest request, Uri uri) async {
  var path = request.uri.path;
  if (path.startsWith('/')) {
    path = path.substring(1);
  }

  var method = request.method;
  // devPrint('requesr: ${request.headers}');

  // devPrint("calling ${method} $uri");
  var client = httpClientFactory.newClient();

  //if (url is String) url = Uri.parse(url);
  var rq = Request(method, uri);

  /*
  if (headers != null) request.headers.addAll(headers);
  if (encoding != null) request.encoding = encoding;
  if (body != null) {
    if (body is String) {
      request.body = body;
    } else if (body is List) {
      request.bodyBytes = DelegatingList.typed(body);
    } else if (body is Map) {
      request.bodyFields = DelegatingMap.typed(body);
    } else {
      throw new ArgumentError('Invalid request body "$body".');
    }
  }

  return Response.fromStream(await send(request));
  */

  // devPrint('1');
  request.headers.forEach((name, List<String> values) {
    // devPrint('2 $name');
    void addHeader() {
      // Don't support multiple values
      if (values.isNotEmpty) {
        rq.headers[name] = values.first;
      }
    }

    if (options?.forwardedHeaders != null) {
      for (var forwardedHeaders in options!.forwardedHeaders!) {
        if (forwardedHeaders.toLowerCase() == name.toLowerCase()) {
          addHeader();
        }
      }
      return;
    }
    if (options?.handleCors ?? true) {
      // don't forward redirect url
      if (name == redirectBaseUrlHeader) {
        return;
      }
      if (options?.containsHeader(name) ?? false) {
        addHeader();
      }
    } else {
      if (name == hostHeader) {
        // don't forward host (likely localhost: 8180)
        // needed for google storage
        return;
      }
      addHeader();
    }
  });
  // Fix to only support gzip
  // and not br for example
  // rq.headers[httpHeaderContentEncoding] = '';
  rq.headers.remove(httpHeaderAcceptEncoding);
  print('headers: ${rq.headers}');
  // rq.contentLength = request.contentLength == null ? -1 : request.contentLength;
  /*
  rq
    ..followRedirects = request.
    ..maxRedirects = request.maxRedirects;
*/
  /*
  List<int> asBytes(body) {
    if (body is String) {
      return utf8.encode(body);
    }
    if (body is List) {
      return body?.cast<int>();
    }
    return null;
  }

   */

  dynamic requestBody = request.body;
  if (requestBody != null) {
    if (requestBody is String) {
      rq.body = requestBody;
    } else if (requestBody is List<int>) {
      rq.bodyBytes = requestBody;
    } else {
      // If content type is null keep it empty
      if (request.headers.contentType != null) {
        print('$requestBody');
        throw 'invalid body format ${(requestBody as Object).runtimeType}';
      }
      // rq.bodyBytes = request.body;
    }
  }
  final rs = await client.send(rq);
  final r = request.response;

  r.statusCode = rs.statusCode;
  print('response: ${r.statusCode}');
  print('respons headers: ${rs.headers}');

  // this crashes: r.contentLength = rs.contentLength;
  rs.headers.forEach((key, value) {
    // This is needed for redirect to work with google
    /*
    String lowercaseKey = key.toLowerCase();

    if (lowercaseKey == 'content-length') {
      return;
    }
    if (lowercaseKey == 'content-encoding') {
      return;
    }
    */

    r.headers.set(key, value);
  });

  /*
  // r.contentLength = rs.contentLength == null ? -1 : rs.contentLength;
  var contentTypeText = rs.headers[httpHeaderContentType];
  if (contentTypeText != null) {
    r.headers.contentType = ContentType.parse(contentTypeText);
  }
  */
  print('fwd response headers: ${r.headers}');
  //r.headers.set(redirectUrlHeader, url);
  //await r.addStream(rs.stream);
  //await r.flush();

  var data = <int>[];
  for (var part in (await rs.stream.toList())) {
    data.addAll(part);
  }

  /*
  const String httpContentTypeHtml = "text/html";
  String logTruncate(String text) {
    int len = 128;
    if (text != null && text.length > len) {
      text = text.substring(0, len);
    }
    return text;
  }
  if (true) {
    devPrint('$httpHeaderContentType: ${r.headers.value(httpHeaderContentType)}');

    if (r.headers.value(httpHeaderContentType)?.contains(httpContentTypeHtml) == true) {
      ;

      devPrint('body: ${hexPretty(listTruncate(data, 256))}');

    }
  }
  */
  r.add(asUint8List(data));

  await r.close();
  print('done');
}

class AppRedirector extends App {
  final String? name;
  final String? baseUrl;

  AppRedirector({this.baseUrl, this.name}) : super();

  @override
  void initDefault() {
    //redirectOptions ??= Options();
    ///context.firebaseFunctions![name!] =
    //    context.firebaseFunctions!.https.onRequest(httpRedirectHandler);
  }
}

class Options {
  bool? handleCors;

  List<String>? forwardedHeaders;

  bool containsHeader(String name) {
    return _lowerCaseCorsHeaders.contains(name.toLowerCase());
  }

  // The default url to redirect too
  //String? baseUrl;

  set corsHeaders(List<String> corsHeaders) {
    _corsHeaders = List.from(corsHeaders);
    _corsHeaders.add(redirectBaseUrlHeader);
    _lowerCaseCorsHeaders = <String>[];
    for (var name in _corsHeaders) {
      _lowerCaseCorsHeaders.add(name.toLowerCase());
    }
  }

  List<String> get corsHeaders => _corsHeaders;

  late List<String> _corsHeaders;
  late List<String> _lowerCaseCorsHeaders;
  String? _corsHeadersText;

  String get corsHeadersText => _corsHeadersText ??= corsHeaders.join(',');
}

class App {
  //String? defaultTargetUrl = defaultRedirectUrl;
  late firebase.App firebaseApp;

  //final Context context;
  Options? redirectOptions;

  //Firestore get firestore => context.firestoreService.firestore(firebaseApp);

  void initDefault() {
    /*
    context.firebaseFunctions![redirectFnName] =
        context.firebaseFunctions!.https.onRequest(httpRedirectHandler);

    firebaseApp = context.firebase!.initializeApp();

     */
  }

  Future httpRedirectHandler(ExpressHttpRequest request) async {
    try {
      //(request) async {
      print('uri: ${request.uri} ${request.method}');
      if (redirectOptions?.handleCors ?? true) {
        //request.response.headers.set(HttpHeaders.CONTENT_TYPE, "text/plain; charset=UTF-8");
        request.response.headers.add('Access-Control-Allow-Methods',
            'POST, OPTIONS, GET, PATCH, DELETE');
        request.response.headers.add('Access-Control-Allow-Origin', '*');

        request.response.headers.add(
            'Access-Control-Allow-Headers',
            // "Origin,Content-Type,Authorization,Accept,connection,content-length,host,user-agent");
            // redirectOptions?.corsHeadersText ??
//                [http_redirect.redirectUrlHeader]
            '*');

        if (request.method == 'OPTIONS') {
          request.response
            ..statusCode = 200
            ..write('');
          unawaited(request.response.close());
          return;
        }
      }
      /*
    var overridenRedirectHostPort =
        parseHostPort(request.headers.value("_tekartik_redirect_host"));
    var redirectHostPort = overridenRedirectHostPort ?? hostPort;
    */
      // compat
      var targetUrl = request.headers.value(http_redirect.redirectUrlHeader);
      targetUrl ??=
          request.uri.queryParameters[http_redirect.redirectUrlHeader];
      request.headers.forEach((name, values) {
        // devPrint('Header: $name = $values');
      });
      if (targetUrl == null) {
        print('no host port');
        request.response
          ..statusCode = 405
          ..write('missing ${http_redirect.redirectBaseUrlHeader} header');
        //  ..flush();
        unawaited(request.response.close());
      } else {
        try {
          await proxyExpressHttpRequest(
              redirectOptions, request, Uri.parse(targetUrl));
        } catch (e, st) {
          print('proxyHttpRequest error $e');
          print('proxyHttpRequest st $st');
          try {
            request.response
                  ..statusCode = 405
                  ..write('caught error $e')
                //..flush()
                ;
            unawaited(request.response.close());
          } catch (_) {}
        }
      }
    } catch (e, st) {
      print(st);
      print(e);
      try {
        request.response.statusCode = 503;
        request.response
            .writeln(json.encode({'error': 'Error $e at ${request.uri}'}));
      } catch (_) {}
      unawaited(request.response.close());
    }
  }
}

// Serve json context
class Context {
  //final FirestoreService firestoreService;
  //final FirebaseFunctions? firebaseFunctions;
  //final HttpClientFactory httpClientFactory;
  //final Firebase? firebase;

  /*
  Context(
      {this.firebaseFunctions,
        this.firebase,
        required this.firestoreService,
        required this.httpClientFactory});*/
}
