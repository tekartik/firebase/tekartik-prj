import 'package:tekartik_http_redirect/http_redirect_client.dart';

var _redirectUri = Uri.parse(
    'https://europe-west3-tekartik-net-dev.cloudfunctions.net/redirectV2');

// Test client
class TestRedirectClient extends RedirectClient {
  TestRedirectClient(super.inner) : super(redirectServerUri: _redirectUri);
}

// Test client
class TestRedirectClientFactory extends RedirectClientFactory {
  TestRedirectClientFactory(super.inner)
      : super(redirectServerUri: _redirectUri);
}
