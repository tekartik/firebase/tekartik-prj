import 'package:tekartik_firebase_firestore/utils/auto_id_generator.dart';
import 'package:tekartik_firebase_functions_node/firebase_functions_universal.dart';
import 'package:tk_gcf_redirect/generate_id_client.dart';
import 'package:tk_gcf_redirect/src/import.dart';

Future generateId(ExpressHttpRequest request) async {
  var autoId = AutoIdGenerator.autoId();

  var params = request.uri.queryParameters;
  var delayMs = int.tryParse((params['delay'] ?? ''));
  if (delayMs != null) {
    await sleep(delayMs);
  }
  var now = DateTime.now().toUtc().toIso8601String();
  await request.response.send((GenerateIdResult()
        ..id.v = autoId
        ..timestamp.v = now)
      .toMap());
}
