import 'dart:async';

import 'package:path/path.dart' hide Context;
import 'package:tekartik_firebase_firestore_sembast/firestore_sembast_io.dart';
import 'package:tekartik_firebase_function_http_redirect/http_redirect.dart'
    as http_redirect;
import 'package:tekartik_firebase_function_http_redirect/io_serve.dart' as fb;
import 'package:tekartik_firebase_function_http_redirect/serve_json.dart'
    as serve_json;
import 'package:tekartik_firebase_functions_io/firebase_functions_io.dart';
import 'package:tekartik_firebase_local/firebase_local.dart';
import 'package:tekartik_http_io/http_client_io.dart';

var deployTopPath = join('..', 'deploy');

var firebaseLocal = FirebaseLocal();

var ioHttpRedirectContext = http_redirect.Context(
    firestoreService: firestoreServiceIo,
    firebaseFunctions: firebaseFunctionsIo,
    firebase: firebaseLocal,
    httpClientFactory: httpClientFactoryIo);
Future main() async {
  //init();

  serve_json.serveJsonMain(serve_json.Context(
    firestoreService: firestoreServiceIo,
    firebaseFunctions: firebaseFunctionsIo,
    firebase: firebaseLocal,
  ));
  var context = ioHttpRedirectContext;
  /*var defaultRedirector =*/
  http_redirect.AppRedirector(context,
      name: 'google_redirect', baseUrl: 'https://www.google.com');

  http_redirect.redirectMain(context);
  unawaited(serve());
  /*var context =*/
  unawaited(fb.firebaseServeIo());

  /*
  var simServer =
      await serve(firebase, ioWebSocketChannelFactory, port: 4996);
  */
}
