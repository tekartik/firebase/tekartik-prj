import 'package:tekartik_http_redirect/http_redirect_cli.dart' as http_redirect;

Future main(List<String> arguments) => http_redirect.main(List.from(arguments)
  ..addAll([
    '--${http_redirect.redirectBaseUrlArgName}',
    'https://storage.googleapis.com/tekartik-free-dev.appspot.com/deploy/basic/index.html'
  ]));
