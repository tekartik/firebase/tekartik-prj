import 'package:tekartik_common_utils/common_utils_import.dart';
import 'package:tekartik_firebase/firebase.dart' hide App;
import 'package:tekartik_firebase_firestore/firestore.dart';
import 'package:tekartik_firebase_functions/firebase_functions.dart';
import 'package:tekartik_http/http.dart';

import 'src/http_redirect_impl.dart';

export 'src/http_redirect_impl.dart' show App, AppRedirector;

const String httpHeaderContentEncoding = 'Content-Encoding';
const String httpHeaderAcceptEncoding = 'Accept-Encoding';

Future helloHandler(ExpressHttpRequest request) async {
  print('request.url ${request.uri}');
  //print("request.originalUrl ${request.requestedUri}");
  request.response.writeln('NEW Hello from Firebase Functions Dart Interop!');
  request.response.writeln('uri: ${request.uri}');
  // request.response.writeln("requestedUri: ${request.requestedUri}");
  request.response.close().unawait();
}

Level? logLevel;

const String redirectBaseUrlHeader = 'x-tekartik-redirect-base-url';

// request
const String hostHeader = 'host';
// response
const String redirectUrlHeader = 'x-tekartik-redirect-url';

// final HttpClient _client = HttpClient();

class Options {
  bool? handleCors;

  List<String>? forwardedHeaders;

  bool containsHeader(String name) {
    return _lowerCaseCorsHeaders.contains(name.toLowerCase());
  }

  // The default url to redirect too
  String? baseUrl;

  set corsHeaders(List<String> corsHeaders) {
    _corsHeaders = List.from(corsHeaders);
    _corsHeaders.add(redirectBaseUrlHeader);
    _lowerCaseCorsHeaders = <String>[];
    for (var name in _corsHeaders) {
      _lowerCaseCorsHeaders.add(name.toLowerCase());
    }
  }

  List<String> get corsHeaders => _corsHeaders;

  late List<String> _corsHeaders;
  late List<String> _lowerCaseCorsHeaders;
  String? _corsHeadersText;

  String get corsHeadersText => _corsHeadersText ??= corsHeaders.join(',');
}

App? app;

void redirectMain(Context context, {Options? redirectOptions}) {
  app = App(context)..redirectOptions = redirectOptions;
}

// Serve json context
class Context {
  final FirestoreService firestoreService;
  final FirebaseFunctions? firebaseFunctions;
  final HttpClientFactory httpClientFactory;
  final Firebase? firebase;

  Context(
      {this.firebaseFunctions,
      this.firebase,
      required this.firestoreService,
      required this.httpClientFactory});
}
