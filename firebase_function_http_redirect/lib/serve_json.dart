import 'package:collection/collection.dart';
import 'package:tekartik_common_utils/common_utils_import.dart';
import 'package:tekartik_common_utils/list_utils.dart';
import 'package:tekartik_firebase/firebase.dart';
import 'package:tekartik_firebase/firebase.dart' as firebase;
import 'package:tekartik_firebase_firestore/firestore.dart';
import 'package:tekartik_firebase_firestore/utils/json_utils.dart';
import 'package:tekartik_firebase_functions/firebase_functions.dart';
import 'package:tekartik_http/http.dart';

void helloHandler(ExpressHttpRequest request) {
  print('request.url ${request.uri}');
  //print("request.originalUrl ${request.requestedUri}");
  request.response.writeln('NEW Hello from Firebase Functions Dart Interop!');
  request.response.writeln('uri: ${request.uri}');
  // request.response.writeln("requestedUri: ${request.requestedUri}");
  request.response.close();
}

class ServeJsonApp {
  late firebase.App firebaseApp;
  final Context context;

  Firestore get firestore => context.firestoreService.firestore(firebaseApp);

  ServeJsonApp(this.context) {
    print('index.main()');
    context.firebaseFunctions!['prj_hello_http'] =
        context.firebaseFunctions!.https.onRequest(helloHandler);
    context.firebaseFunctions!['prj_firestore_json'] =
        context.firebaseFunctions!.https.onRequest(firestoreHttpHandler);

    firebaseApp = context.firebase!.initializeApp();
  }

  void _waitForChange(ExpressHttpRequest request, String path,
      Map<String, dynamic>? originalJsonMap) {
    late StreamSubscription<DocumentSnapshot> subscription;
    subscription =
        firestore.doc(path).onSnapshot().listen((DocumentSnapshot snapshot) {
      var jsonMap = documentDataToJsonMap(documentDataFromSnapshot(snapshot));
      if (!const DeepCollectionEquality().equals(jsonMap, originalJsonMap)) {
        request.response.writeln(json.encode(jsonMap));
        request.response.close();
        subscription.cancel();
      }
    });
  }

  Future firestoreHttpHandler(ExpressHttpRequest request) async {
    var path = request.uri.queryParameters['path'];
    var change = parseBool(listFirst(request.headers['change'])) ?? false;
    var dev = parseBool(listFirst(request.headers['dev'])) ?? false;
    if (dev) {
      print('${request.method} ${request.uri}');
      // print(request.headers);
      print('change: $change');
    }
    try {
      if ((request.method == httpMethodGet) || change) {
        if (path == null) {
          request.response.statusCode = 405;
          request.response
              .writeln(json.encode({'error': 'Missing path param'}));
          request.response.close().unawait();
          return;
        }
        var data = documentDataToJsonMap(
            documentDataFromSnapshot(await firestore.doc(path).get()));
        if (data == null) {
          if (change) {
            // look for changes
            var originalJsonMap = getRequestJsonMap(request);
            if (originalJsonMap == null) {
              _waitForChange(request, path, originalJsonMap);
              return;
            }
          }
          request.response.statusCode = 404;
          request.response
              .writeln(json.encode({'error': 'Data not found at $path'}));
          request.response.close().unawait();
          return;
        } else {
          if (change) {
            var originalJsonMap = getRequestJsonMap(request);
            // devPrint('compating $originalJsonMap $data ${originalJsonMap.runtimeType} ${data.runtimeType}');
            if (const DeepCollectionEquality().equals(data, originalJsonMap)) {
              _waitForChange(request, path, originalJsonMap);
              return;
            }
          }
        }

        // documentDataToJsonMap(data);

        print('path: $path');
        // request.

        // print("request.originalUrl ${request.requestedUri}");
        request.response.writeln(json.encode(data));
        request.response.close().unawait();
      } else if (request.method == httpMethodPut) {
        var jsonMap = getRequestJsonMap(request);
        // devPrint('data: $jsonMap');
        var data = documentDataFromJsonMap(firestore, jsonMap)!;
        await firestore.doc(path!).set(data.asMap());
        request.response.close().unawait();
      } else {
        throw 'unsupported ${request.method}';
      }
    } catch (e, st) {
      print(st);
      print(e);
      request.response.statusCode = 503;
      request.response.writeln(json.encode({'error': 'Error $e at $path'}));
      request.response.close().unawait();
    }
  }

  Map<String, dynamic>? getRequestJsonMap(ExpressHttpRequest request) {
    // devPrint(request.body);
    // devPrint(request.body?.runtimeType);
    /*
    devPrint('1 length ${request.contentLength}');
    if (request.contentLength != 0) {
      devPrint('2');
      List<int> bodyData = [];
      var completer = Completer();
      request.listen((List<int> data) {
        print(data);
        bodyData.addAll(data);
        if (request.contentLength != 1 && bodyData.length >= request.contentLength) {
          completer.complete();
        }
      });
      await completer.future;
      String body = utf8.decode(bodyData); //await request.map(utf8.decode).join();
      devPrint('3');
      // var body = await utf8.decodeStream(request);
      print(body);
      Map<String, dynamic> jsonMap = (json.decode(body) as Map)?.cast<
          String,
          dynamic>();
      return jsonMap;
    }
    devPrint('3');
    return null;
    */
    return requestBodyAsJsonObject(request.body);
  }
}

ServeJsonApp? app;

void serveJsonMain(Context context) {
  app = ServeJsonApp(context);
}

// Serve json context
class Context {
  final FirestoreService firestoreService;
  final FirebaseFunctions? firebaseFunctions;
  final Firebase? firebase;

  Context(
      {this.firebaseFunctions, this.firebase, required this.firestoreService});
}
