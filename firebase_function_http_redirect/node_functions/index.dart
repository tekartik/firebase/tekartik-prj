import 'package:tekartik_firebase_firestore_node/firestore_node.dart';
import 'package:tekartik_firebase_function_http_redirect/http_redirect.dart';
import 'package:tekartik_firebase_function_http_redirect/http_redirect.dart'
    as http_redirect;
import 'package:tekartik_firebase_functions_node/firebase_functions_node.dart';
import 'package:tekartik_firebase_node/firebase_node.dart';
import 'package:tekartik_http_node/http_client_node.dart';

void main() {
  // node_interop.fs;

  //init();
  var context = Context(
    firestoreService: firestoreServiceNode,
    firebaseFunctions: firebaseFunctionsNode,
    firebase: firebaseNode,
    httpClientFactory: httpClientFactoryNode,
  );

  http_redirect.AppRedirector(context,
      name: 'google_redirect', baseUrl: 'https://www.google.com');
}
