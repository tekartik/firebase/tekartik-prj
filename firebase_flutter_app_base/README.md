## All in one

Firebase and simulator client for the web and io

```yaml
dependencies:
  tekartik_firebase_flutter_app_base:
    git:
      url: git@gitlab.com:tekartik/firebase/tekartik-prj
      path: firebase_flutter_app_base
      ref: dart3a
    version: '>=0.1.0'
```

## Test