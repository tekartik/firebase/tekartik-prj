import 'dart:html';
// ignore_for_file: avoid_web_libraries_in_flutter
// ignore: depend_on_referenced_packages
import 'package:tekartik_browser_utils/css_utils.dart';
import 'package:tekartik_firebase_flutter_app_base/menu/init.dart';

Future preMenuInit() async {
  //throw UnimplementedError('preMenuInit');
  //  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
  // document.head.children.add(CssS());
  await loadStylesheet(
      'https://fonts.googleapis.com/css?family=Roboto&display=swap');

  //   <link rel="icon" href="packages/tekartik_browser_utils/img/favicon.png">
  document.head!.children.add(LinkElement()
    ..rel = 'icon'
    ..href = 'packages/tekartik_browser_utils/img/favicon.png');
  await appInfoInit(packageName: appPackageName);
}

/*
Future loadStyle(String content) {
  Completer completer = Completer();
  var link = LinkElement();
  link.type = 'text/css';
  link.onError.listen((e) {
    // This is actually the only callback called upon success
    // onError, onDone are never called
    completer.completeError(Exception('stylesheet $src not loaded'));
  });
  link.onLoad.listen((_) {
    // This is actually the only callback called upon success
    // onError, onDone are never called
    completer.complete();
  });
  link.href = src;
  link.rel = "stylesheet";
  document.head.children.add(link);
  return completer.future;
}
*/
