import 'package:tekartik_firebase_flutter_app_base/app_host/app_host.dart';

Future preMenuInit() async {
  // Fine for android/ios
  // throw UnimplementedError('preMenuInit');
  await appInfoInit(packageName: appPackageName);
}
