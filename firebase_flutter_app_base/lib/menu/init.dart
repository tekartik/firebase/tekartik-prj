import 'package:tekartik_firebase_flutter_app_base/app_host/app_host_impl.dart';

export 'package:tekartik_firebase_flutter_app_base/app_host/app_host.dart';

export 'init_import.dart';

bool? _showConsole;

bool? get menuShowConsole {
  assert(prefs != null, 'init prefs first');
  _showConsole ??= prefs!.getBool('showConsole');
  return _showConsole;
}

set menuShowConsole(bool? showConsole) {
  assert(prefs != null, 'init prefs first');
  _showConsole = showConsole;
  prefs!.setBool('showConsole', showConsole);
}
