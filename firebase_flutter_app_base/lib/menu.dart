// ignore_for_file: depend_on_referenced_packages

import 'package:flutter/material.dart';
import 'package:tekartik_firebase_flutter_app_base/menu/init.dart';
import 'package:tekartik_test_menu_flutter/test_menu_flutter.dart';
import 'package:tekartik_test_menu_flutter/test_menu_flutter.dart' as impl;

import 'menu/init.dart' as impl;

export 'package:tekartik_test_menu/test_menu.dart' show prompt;
export 'package:tekartik_test_menu_flutter/test_menu_flutter.dart'
    hide initTestMenuFlutter, mainMenu;

Future menuFlutterPreInit() => impl.preMenuInit();

Future initTestMenuFlutter(
    {Widget Function(Widget child)? builder, bool? showConsole}) async {
  await menuFlutterPreInit();
  if (menuShowConsole != null) {
    showConsole = menuShowConsole;
  }
  impl.initTestMenuFlutter(builder: builder, showConsole: showConsole);
}

void menuSettings() {
  menu('Settings', () {
    item('Show console', () {
      menuShowConsole = true;
      write('restart to show console');
    });
    item('Hide console', () {
      menuShowConsole = false;
      write('restart to hide console');
    });
  });
}

Future mainMenu(void Function() body, {bool? showConsole}) async {
  await initTestMenuFlutter(
      builder: (Widget child) {
        // _testMenuManagerFlutter.bodyBuilder = body;
        return Builder(builder: (BuildContext context) {
          // devPrint('Building tests');
          body();
          menuSettings();
          return child;
        });
      },
      showConsole: showConsole);
}
