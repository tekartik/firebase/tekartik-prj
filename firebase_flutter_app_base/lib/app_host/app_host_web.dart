import 'package:tekartik_app_utils/app_host_target_browser.dart'; // ignore: depend_on_referenced_packages
import 'package:tekartik_firebase_client_browser_base/net/net_context_sim_browser.dart';
import 'package:tekartik_firebase_flutter_app_base/app_host/app_host.dart';
import 'package:tekartik_firebase_flutter_app_base/app_host/app_host_impl.dart';
import 'package:tekartik_test_menu_flutter/test_menu_flutter.dart';

AppInfoImpl? appInfoImpl;

Future<AppInfo> preAppHostInit() async {
  return await appInfoInitSim();
}

Future<AppInfoImpl> preAppHostInitCommon() async {
  try {
    prefs = await prefsReady;
  } catch (e) {
    write(e);
  }
  if (appInfoImpl == null) {
    AppInfo appInfo = AppInfoImpl();
    appInfo.prefs = prefs;
    appInfo.target = AppHostTarget.fromLocationInfo(locationInfo);
    appInfoImpl = appInfo as AppInfoImpl?;
  }
  return appInfoImpl!;
}

Future<AppInfo> appInfoInitSim() async {
  if (appInfoImpl == null) {
    var appInfo = await preAppHostInitCommon();
    appInfo.netContext = netContextSimBrowser;
  }
  return appInfoImpl!;
}

/*
Future<AppInfo> preAppHostInitBrowser() async {
  if (appInfoImpl == null) {
    var appInfo = await preAppHostInitCommon();
    appInfo.netContext = netContextBrowser;
    return appInfo;
  }
  return appInfoImpl!;
}*/

/*
Future loadStyle(String content) {
  Completer completer = Completer();
  var link = LinkElement();
  link.type = 'text/css';
  link.onError.listen((e) {
    // This is actually the only callback called upon success
    // onError, onDone are never called
    completer.completeError(Exception('stylesheet $src not loaded'));
  });
  link.onLoad.listen((_) {
    // This is actually the only callback called upon success
    // onError, onDone are never called
    completer.complete();
  });
  link.href = src;
  link.rel = "stylesheet";
  document.head.children.add(link);
  return completer.future;
}
*/
