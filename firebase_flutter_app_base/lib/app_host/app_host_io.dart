import 'package:flutter/material.dart';
import 'package:tekartik_firebase_client_browser_base/net/net_context_io.dart';
import 'package:tekartik_firebase_flutter_app_base/app_host/app_host.dart';
import 'package:tekartik_firebase_flutter_app_base/app_host/app_host_impl.dart';
import 'package:tekartik_firebase_flutter_app_base/src/import.dart';

Future<AppInfo> preAppHostInit() async {
  WidgetsFlutterBinding.ensureInitialized();
  try {
    prefs = await prefsReady;
  } catch (e) {
    write(e);
    // print(e);
  }

  AppInfo appInfo = AppInfoImpl();
  appInfo.netContext = netContextIo;
  appInfo.prefs = prefs;
  if (isDebug) {
    appInfo.target = AppHostTarget.dev;
  } else {
    appInfo.target = AppHostTarget.prod;
  }
  return appInfo;
}
