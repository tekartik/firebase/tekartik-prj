import 'package:tekartik_app_prefs/app_prefs.dart';

import 'package:tekartik_firebase_client_browser_base/net/net_context.dart';
import 'package:tekartik_firebase_flutter_app_base/src/import.dart';

import 'app_host_impl.dart' as impl;

export 'app_host_stub.dart'
// ignore: uri_does_not_exist
    if (dart.library.html) 'app_host_web.dart'
// ignore: uri_does_not_exist
    if (dart.library.io) 'app_host_io.dart';

String? appPackageName;

abstract class AppInfo {
  String? get packageName;

  AppHostTarget? target;
  NetContext? netContext;
  Prefs? prefs;
}

AppInfo? appInfo;
Future<AppInfo>? appInfoReady;

/// Must be set for webApp, debug prefix for ios/android app, otherwise ignored
Future<AppInfo> appInfoInit({required String? packageName}) async {
  appInfoReady = impl.appInfoInit(packageName: packageName);
  appInfo = await appInfoReady;
  return appInfo!;
}
