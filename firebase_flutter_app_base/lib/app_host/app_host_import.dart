import 'package:tekartik_firebase_flutter_app_base/src/import.dart';

abstract class AppInfo {
  String get packageName;

  AppHostTarget? target;
  bool? showConsole;
}
