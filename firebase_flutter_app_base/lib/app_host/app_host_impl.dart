import 'package:tekartik_app_prefs/app_prefs.dart';
import 'package:tekartik_firebase_client_browser_base/net/net_context.dart';
import 'package:tekartik_firebase_flutter_app_base/src/import.dart';

import 'app_host.dart';

Future<AppInfo> appInfoInit({required String? packageName}) async {
  return await (preAppHostInit());
}

final _prefsMemoizer = AsyncMemoizer<Prefs?>();

Future<Prefs?> get prefsReady => _prefsMemoizer.runOnce(() async {
      assert(appPackageName != null, 'init appPackageName first');
      prefs = await prefsFactory.openPreferences(appPackageName!);
      assert(prefs != null, 'init prefs first');
      return prefs;
    });

Prefs? prefs;

class AppInfoImpl implements AppInfo {
  @override
  NetContext? netContext;

  @override
  Prefs? prefs;

  @override
  AppHostTarget? target;

  @override
  String? packageName;
}
