import 'package:path/path.dart';
import 'package:tekartik_build_utils/flutter/app/generate.dart';

String firebaseauthAppDirName = join('..', 'firebase_flutter_app_example');
Future main() async {
  await generate();
}

Future generate({bool? force}) async {
  await gitGenerate(
      dirName: firebaseauthAppDirName,
      appName: 'tekartik_firebase_flutter_app_example',
      force: force);
}
