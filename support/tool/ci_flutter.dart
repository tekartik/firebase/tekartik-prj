import 'package:process_run/shell.dart';

Future main() async {
  var shell = Shell().cd('..');

  for (var dir in [
    'firebase_flutter_app_example',
  ]) {
    shell = shell.pushd(dir);
    await shell.run('''
    
  flutter packages get
  dart tool/ci.dart
  
''');
    shell = shell.popd();
  }
}
