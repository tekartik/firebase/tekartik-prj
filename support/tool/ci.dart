import 'package:process_run/shell.dart';

Future main() async {
  var shell = Shell().cd('..');

  for (var dir in [
    'firebase_client_browser_base',
  ]) {
    shell = shell.pushd(dir);
    await shell.run('''
    
  pub get
  dart tool/ci.dart
  
''');
    shell = shell.popd();
  }
}
