# Request

```
curl https://europe-west3-tekartik-net-dev.cloudfunctions.net/redirectV2 -H "x-tekartik-redirect-url: https://europe-west3-tekartik-net-dev.cloudfunctions.net/generateIdV2"

{"id":"eO46g7EyTUtv3CsTY7Rb","timestamp":"2022-06-09T18:51:05.082Z"}

curl https://europe-west3-tekartik-net-dev.cloudfunctions.net/redirectV2 \
  -H "x-tekartik-redirect-url: https://europe-west3-tekartik-net-dev.cloudfunctions.net/generateIdV2"
  
{"id":"eO46g7EyTUtv3CsTY7Rb","timestamp":"2022-06-09T18:51:05.082Z"}

curl https://europe-west3-tekartik-net-dev.cloudfunctions.net/redirectV2 \
  -X OPTIONS \
  -H "x-tekartik-redirect-url: https://europe-west3-tekartik-net-dev.cloudfunctions.net/generateIdV2"

```