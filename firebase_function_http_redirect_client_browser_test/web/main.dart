import 'package:http/http.dart';
// ignore: depend_on_referenced_packages
import 'package:tekartik_app_http/app_http.dart';
import 'package:tekartik_http_redirect/http_redirect.dart';
import 'package:tekartik_http_redirect/http_redirect_client.dart';
import 'package:tekartik_test_menu_browser/test_menu_mdl_browser.dart';
import 'package:tk_gcf_redirect/http_redirect_client.dart';

var region = 'europe-west3';
var projectId = 'tekartik-net-dev';
//import '

final localDartRedirectClientFactory = RedirectClientFactory(httpClientFactory,
    redirectServerUri: Uri.parse('http://localhost:4999/redirectV2'));
final localNodeRedirectClientFactory = RedirectClientFactory(httpClientFactory,
    redirectServerUri: Uri.parse(
        'http://localhost:5000/tekartik-net-dev/europe-west3/redirectV2'));
final redirectClientFactory = TestRedirectClientFactory(httpClientFactory);

Future<void> main() async {
  await initTestMenuBrowser();

  menu('main', () {
    Future<String> testUri(Uri uri) async {
      write('uri: $uri');
      var result = await read(uri);
      write(result);
      return result;
    }

    item('rest generatedIdv2', () async {
      testUri(Uri.parse(
          'https://europe-west3-tekartik-net-dev.cloudfunctions.net/generateIdV2'));
    });
    item('rest echoQuery', () async {
      testUri(Uri.parse(
          'https://europe-west3-tekartik-net-dev.cloudfunctions.net/echoQuery?test=1'));
    });

    Future<String> testLocalDartRedirectUri(Uri uri) async {
      var client = localDartRedirectClientFactory.newClient();
      write('through: ${localDartRedirectClientFactory.redirectServerUri}');
      write('uri: $uri');
      var result = await client.read(uri);
      write(result);
      return result;
    }

    Future<String> testLocalNodeRedirectUri(Uri uri) async {
      var client = localDartRedirectClientFactory.newClient();
      write('through: ${localDartRedirectClientFactory.redirectServerUri}');
      write('uri: $uri');
      var result = await client.read(uri);
      write(result);
      return result;
    }

    Future<String> testRedirectUri(Uri uri) async {
      var client = redirectClientFactory.newClient();
      write('through: ${redirectClientFactory.redirectServerUri}');
      write('uri: $uri');
      var result = await client.read(uri);
      write(result);
      return result;
    }

    item('local dart generatedIdv2', () async {
      await testUri(Uri.parse('http://localhost:4999/generateIdV2'));
    });
    item('rest redirectV2', () async {
      var uri = Uri.parse(
              'https://europe-west3-tekartik-net-dev.cloudfunctions.net/redirectV2')
          .replace(queryParameters: {
        redirectUrlHeader:
            'https://europe-west3-tekartik-net-dev.cloudfunctions.net/generateIdV2'
      });
      write(uri);
      var result = await read(uri);
      write(result);
    });
    item('rest redirectV2 tka2k', () async {
      var uri = Uri.parse(
              'https://europe-west3-tekartik-net-dev.cloudfunctions.net/redirectV2')
          .replace(queryParameters: {
        redirectUrlHeader:
            'http://82.165.102.240:8080/api/getlistfiles?type=media_photo'
      });
      write(uri);
      var result = await read(uri);
      write(result);
    });

    item('rest redirect using api', () async {
      await testRedirectUri(Uri.parse(
          'https://europe-west3-tekartik-net-dev.cloudfunctions.net/generateIdV2'));
    });
    item('rest redirectV2 using API tka2k', () async {
      await testRedirectUri(Uri.parse(
          'http://82.165.102.240:8080/api/getlistfiles?type=media_photo'));
    });

    item('local dart redirect using api', () async {
      var client = RedirectClient(Client(),
          redirectServerUri: Uri.parse('http://localhost:4999/redirectV2'));
      var result = await client.read(Uri.parse(
          'https://europe-west3-tekartik-net-dev.cloudfunctions.net/generateIdV2'));
      write(result);
    });
    item('local node generatedIdv2', () async {
      var result = await read(
          Uri.parse('http://localhost:5000/$projectId/$region/generateIdV2'));
      write(result);
    });
    item('local dart generatedIdv2', () async {
      var result = await read(Uri.parse('http://localhost:4999/generateIdV2'));
      write(result);
    });
    item('local dart redirectV2', () async {
      var uri = Uri.parse('http://localhost:4999/redirectV2').replace(
          queryParameters: {
            redirectUrlHeader: 'http://localhost:4999/generateIdV2'
          });
      await testUri(uri);
    });
    item('local dart redirectV2 using headers', () async {
      var uri = Uri.parse('http://localhost:4999/redirectV2');
      var headers = {redirectUrlHeader: 'http://localhost:4999/generateIdV2'};

      write('headers: $headers');
      await testUri(uri);
    });
    item('local dart redirectV2 using API', () async {
      await testLocalDartRedirectUri(
          Uri.parse('http://localhost:4999/generateIdV2'));
    });
    item('local dart redirectV2 using API tka2k', () async {
      await testLocalDartRedirectUri(Uri.parse(
          'http://82.165.102.240:8080/api/getlistfiles?type=media_photo'));
    });
    item('local node redirectV2', () async {
      var result = await read(
          Uri.parse('http://localhost:5000/$projectId/$region/redirectV2')
              .replace(queryParameters: {
        redirectUrlHeader:
            'http://localhost:5000/$projectId/$region/generateIdV2'
      }));
      write(result);
    });
    item('local node redirectV2 using API', () async {
      await testLocalNodeRedirectUri(Uri.parse(
          'http://82.165.102.240:8080/api/getlistfiles?type=media_photo'));
    });
  });
}
