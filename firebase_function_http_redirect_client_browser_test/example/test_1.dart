import 'package:tk_gcf_redirect/http_redirect_client.dart';
// ignore: depend_on_referenced_packages
import 'package:tekartik_app_http/app_http.dart';

Future<void> main() async {
  final redirectClientFactory = TestRedirectClientFactory(httpClientFactory);
  Future<String> testRedirectUri(Uri uri) async {
    var client = redirectClientFactory.newClient();
    print('through: ${redirectClientFactory.redirectServerUri}');
    print('uri: $uri');
    var result = await client.read(uri);
    print(result);
    return result;
  }

  await testRedirectUri(Uri.parse(
      'https://europe-west3-tekartik-net-dev.cloudfunctions.net/generateIdV2'));
}
