//import 'package:firebase_auth/firebase_auth.dart';
// ignore_for_file: depend_on_referenced_packages

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_ui_auth/firebase_ui_auth.dart';
import 'package:firebase_ui_oauth_google/firebase_ui_oauth_google.dart';
import 'package:flutter/material.dart';

import 'firebase_options.dart';

var googleCliendId =
    '29718539056-ttushfi8ns1vcgvvhvhog22j6cgo7536.apps.googleusercontent.com';

var configurable = true; //  kIsWeb || (!Platform.isLinux);
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  if (configurable) {
    await Firebase.initializeApp(
        options: DefaultFirebaseOptions.currentPlatform);
  }

  FirebaseUIAuth.configureProviders([GoogleProvider(clientId: googleCliendId)]);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute:
          //FirebaseAuth.instance.currentUser == null ? '/sign-in' : '/profile',
          // '/profile',
          '/sign-in',
      routes: {
        '/sign-in': (context) {
          return SignInScreen(
            // providers: providers,
            actions: [
              AuthStateChangeAction<SignedIn>((context, state) {
                Navigator.pushReplacementNamed(context, '/profile');
              }),
            ],
          );
        },
        '/profile': (context) {
          return ProfileScreen(
            //providers: providers,
            actions: [
              SignedOutAction((context) {
                Navigator.pushReplacementNamed(context, '/sign-in');
              }),
            ],
          );
        },
      },
    );
  }
}
