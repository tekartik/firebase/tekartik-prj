import 'dart:io';

import 'package:process_run/shell.dart';
import 'package:dev_build/menu/menu_io.dart';

var keystore = 'firebase_exp.jks';
var password = 'dVmPw7rtMeSmEthbAxe6nBR2rJ3xwzAw';

Future main(List<String> arguments) async {
  mainMenuConsole(arguments, () {
    menu('android', () {
      menu('one', () {
        item('genkey', () async {
          // ignore: avoid_print
          print(
              'keytool -genkey -v -keystore android/app/$keystore -storepass $password -alias app -keypass $password -keyalg RSA -keysize 2048 -validity 100000');
        });
        item('write keystore.properties', () async {
          await File('android/app/keystore.properties').writeAsString('''
keyAlias=app
keyPassword=$password
storeFile=$keystore
storePassword=$password
          ''');
        });
        item('info signing', () async {
          var shell = Shell();
          await shell.run(
              'keytool -list -v -keystore android/app/$keystore -storepass $password');
          // $ keytool -list -v -keystore android/app/firebase_exp.jks -storepass dVmPw7rtMeSmEthbAxe6nBR2rJ3xwzAw
          // Keystore type: PKCS12
          // Keystore provider: SUN
          //
          // Your keystore contains 2 entries
          //
          // Alias name: app
          // Creation date: Nov 3, 2022
          // Entry type: PrivateKeyEntry
          // Certificate chain length: 1
          // Certificate[1]:
          // Owner: CN=Alexandre Roux, OU=Unknown, O=Tekartik, L=Ledignan, ST=Unknown, C=FR
          // Issuer: CN=Alexandre Roux, OU=Unknown, O=Tekartik, L=Ledignan, ST=Unknown, C=FR
          // Serial number: 65ae343f
          // Valid from: Thu Nov 03 16:50:28 CET 2022 until: Tue Aug 18 17:50:28 CEST 2296
          // Certificate fingerprints:
          // 	 SHA1: 29:12:17:B9:60:8C:D9:FA:F0:18:34:94:39:97:04:84:DF:E6:CE:24
          // 	 SHA256: 5D:FF:5E:6C:68:1B:60:2E:A3:12:EC:70:33:CF:60:25:91:B4:7C:E5:C6:04:8F:6A:78:77:84:69:3E:7C:F6:77
          // Signature algorithm name: SHA256withRSA
          // Subject Public Key Algorithm: 2048-bit RSA key
          // Version: 3
        });
      });
      /*
      item('clean build', () async {
        await project.main(clean: true);
      });
      item('build', () async {
        await project.main();
      });
      item('build all debug', () async {
        await project.runGradlew('assembleDebug');
      });*/
    });
  });
}
